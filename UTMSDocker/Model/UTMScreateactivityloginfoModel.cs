﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UTMScreateactivityloginfoModel
    {

        public class UTMScreateactivityloginfoInput
        {
            public int? created_by { get; set; }
            public string module_name { get; set; }
            public DateTime? create_date { get; set; }
            public string comments { get; set; }
        }
        public class UTMScreateactivityloginfoOutput : ErrorMessageModel
        {
            
        }
    }
}