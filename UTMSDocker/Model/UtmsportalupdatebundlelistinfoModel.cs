﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmsportalupdatebundlelistinfoModel
    {
        public class UtmsportalupdatebundlelistinfoInput
        {
            public int bundleid { get; set; }
            public int bundle_status { get; set; }
            public string bundle_name { get; set; }
            public double? price { get; set; }
            public double? national_min { get; set; }
            public double? international_min { get; set; }
            public int bundle_type { get; set; }
            public int reseller_flag { get; set; }
        }
    public class UtmsportalupdatebundlelistinfoOutput:ErrorMessageModel
        {


        }

    }
}