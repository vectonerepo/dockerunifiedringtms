﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmsgetresellerdiscountinfoModel
    {
        public class UtmsgetresellerdiscountinfoInput
        {
            public int pc_product { get; set; }
        }
        public class UtmsgetresellerdiscountinfoOutput:ErrorMessageModel
        {
            public int? user_range_from { get; set; }
            public int? user_range_to { get; set; }
            public double? discount { get; set; }
        }
    }
}