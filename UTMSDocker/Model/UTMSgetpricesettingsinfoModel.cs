﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UTMSgetpricesettingsinfoModel
    {
        public class UTMSgetpricesettingsinfoOutput : ErrorMessageModel
        {
            public int? pricing_id { get; set; }
            public string pricing_status { get; set; }
            
        }
    }
}