﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmsplanpricingcreatenotesinfoModel
    {

        public class UtmsplanpricingcreatenotesinfoInput
        {
            public int plan_id { get; set; }
            public int processtype { get; set; }
            public string plan_notes { get; set; }
            public int plan_notes_status { get; set; }
            public int plan_notes_id { get; set; }
        }
        public class UtmsplanpricingcreatenotesinfoOutput:ErrorMessageModel
        {

        }


    }
}