﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UTMSgetplaninfoModel
    {
       public class UTMSgetplaninfoInput
        {
            public int? Product_ID { get; set; }
            public int? Plan_id { get; set; }
        }
        public class UTMSgetplaninfoOutput : ErrorMessageModel
        {
            public int? PC_Idx { get; set; }
            public string Bundle_Type { get; set; }
            public string plan_name { get; set; }
            public double? PC_Price { get; set; }
            public int? duration { get; set; }
            public int? Request_ID { get; set; }
            public string features { get; set; }
            public int? National_min { get; set; }
            public int? Geo_Min { get; set; }
            public int? Unlimted_Intra_Call { get; set; }
            public int? is_free_trial { get; set; }
        }
    }
}