﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class RequestTokenOutput
    {
        public string AccessToken { get; set; }
        public string Message { get; set; }
        public int Code { get; set; }
    }
    public class RequestTokenInput
    {
        public string SecretId { get; set; }
        public string SecretPassword { get; set; }
    }
}