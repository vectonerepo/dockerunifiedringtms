﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmsgetwebsratescountrysummaryinfoModel
    {
        public class UtmsgetwebsratescountrysummaryinfoInput
        {
            public string callingtype { get; set; }
        }
        public class UtmsgetwebsratescountrysummaryinfoOutput:ErrorMessageModel
        {
            public string calling_type { get; set; }
            public string Type { get; set; }
            public string country { get; set; }
            public int? prefix_count { get; set; }
        }
    }
}