﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmsgetdownloadedfilenameModel
    {
        public class UtmsgetdownloadedfilenameInput
        {
            public string callingtype { get; set; }
        }
        public class UtmsgetdownloadedfilenameOutput:ErrorMessageModel
        {
            public string filename { get; set; }
        }

    }
}