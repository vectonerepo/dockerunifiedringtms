﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmsrolemenumappingModel
    {
        public class UtmsrolemenumappingInput
        {
            public int Role_ID { get; set; }
            public string menu_id { get; set; }
        }
        public class UtmsrolemenumappingOutput :ErrorMessageModel
        {
        }
    }
}