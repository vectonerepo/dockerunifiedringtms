﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmscreatecategoryinfoModel
    {
        public class UtmscreatecategoryinfoInput
        {
            public string Category_Name { get; set; }
            public int process_type { get; set; }
            public int Category_id { get; set; }
            public int status { get; set; }
            public int category_position { get; set; }
        }
        public class UtmscreatecategoryinfoOutput: ErrorMessageModel
        {

        }
    }
}