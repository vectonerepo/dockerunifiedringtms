﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmscompanybulkuploadcallingratedtlinfoModel
    {
        public class UtmscompanybulkuploadcallingratedtlinfoInput
        {
            public int processtype { get; set; }
            public string in_xml_data { get; set; }
        }
        public class UtmscompanybulkuploadcallingratedtlinfoOutput:ErrorMessageModel
        {
        }
    }
}