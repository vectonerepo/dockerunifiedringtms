﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UTMScreateplaninfoModel
    {

        public class UTMScreateplaninfoInput
        {
            public int? User_Id { get; set; }
            public int? Product_Id { get; set; }
            public int? duration { get; set; }
            public int? Pc_From_Users { get; set; }
            public int? Pc_To_Users { get; set; }
            public double? PC_Price { get; set; }
            public string features { get; set; }
            public int? Process_Type { get; set; }
            public int? Plan_ID { get; set; }
            public int? National_min { get; set; }
            public int? Geo_Min { get; set; }
            public string  Plan_Name { get; set; }
            public int? Unlimted_Intra_Call { get; set; }
            
        }
        public class UTMScreateplaninfoOutput : ErrorMessageModel
        { 
                public int? is_free_trial { get; set; }
        }
    }
}