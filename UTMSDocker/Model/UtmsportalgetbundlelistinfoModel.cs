﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmsportalgetbundlelistinfoModel
    {
        public class UtmsportalgetbundlelistinfoInput
        {
            public int bundleid { get; set; }
        }
        public class UtmsportalgetbundlelistinfoOutput : ErrorMessageModel
        {
            public int? id { get; set; }
            public int? bundleid { get; set; }
            public string bundle_name { get; set; }
            public double? price { get; set; }
            public double? national_min { get; set; }
            public double? international_min { get; set; }
            public int? bundle_status { get; set; }
            public int? bundle_type { get; set; }
            public int? Reseller_flag { get; set; }
            public int? days_valid { get; set; }
            public int? renewal_mode { get; set; }
            public string renewal_status { get; set; }
            public string tariffclass { get; set; }
            public string sms_tariff { get; set; }
            public int? reset_mode { get; set; }
            public double? sms_allowance { get; set; }
            public int? Schedule_Type { get; set; }
            public DateTime? schedule_time { get; set; }
        }

    }
}