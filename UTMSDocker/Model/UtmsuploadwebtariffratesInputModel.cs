﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmsuploadwebtariffratesInputModel
    {
        public class UtmsuploadwebtariffratesInput
        {
            public int callingtype { get; set; }
            public string filename { get; set; }
            public string login_by { get; set; }
        }
        public class UtmsuploadwebtariffratesOutput:ErrorMessageModel
        {
            
        }

    }
}