﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmsportalsmesubscribebundleespModel
    {
        public class UtmsportalsmesubscribebundleespInput
        {
            public int company_id { get; set; }
            public string sitecode { get; set; }
            public int enterpriseid { get; set; }
            public int bundleid { get; set; }
            public int paymode { get; set; }
            public string processby { get; set; }
        }
        public class UtmsportalsmesubscribebundleespOutput:ErrorMessageModel
        {

        }
    }
}