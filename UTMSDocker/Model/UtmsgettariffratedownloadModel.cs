﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmsgettariffratedownloadModel
    {
        public class UtmsgettariffratedownloadInput
        {
            public string trffclass { get; set; }
            public int accesscharge { get; set; }
        }
        public class UtmsgettariffratedownloadOutput:ErrorMessageModel
        {
            public string destcode { get; set; }
            public int? charge { get; set; }
            public int? timeperiod { get; set; }
            public int? cnxdelay { get; set; }
            public double? cnxprice { get; set; }
            public int? sampdelay { get; set; }
            public double? pricemin { get; set; }
            public int? premiumdest { get; set; }
            public int? freesec { get; set; }
        }
    }
}