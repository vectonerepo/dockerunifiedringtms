﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class utmsplanpricinggetnotesinfoModel
    {
        public class utmsplanpricinggetnotesinfoInput
        {
            public int plan_id { get; set; }
            public int plan_notes_id { get; set; }

        }
        public class utmsplanpricinggetnotesinfoOutput:ErrorMessageModel
        {
            public int plan_notes_id { get; set; }
            public int plan_id { get; set; }
            public string plan_notes { get; set; }
            public int plan_notes_status { get; set; }
            public DateTime create_date { get; set; }
        }

    }
}