﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmsgetsmeplandetailsModel
    {

        public class UtmsgetsmeplandetailsInput
        {
            public int? Reseller_ID { get; set; }
            public int? Product_Id { get; set; }
            public int? Plan_id { get; set; }
            public int? is_tms_flag { get; set; }
        }
        public class UtmsgetsmeplandetailsOutput : ErrorMessageModel
        {
            public string Bundle_Type { get; set; }
            public string plan_name { get; set; }
            public int? PC_Product { get; set; }
            public string PC_Price { get; set; }
            public string features { get; set; }
            public int? National_min { get; set; }
            public int? Geo_Min { get; set; }
            public int? Request_ID { get; set; }
            public int? Unlimted_Intra_Call { get; set; }
            public int? is_free_trial { get; set; }
            public int? PC_most_popular { get; set; }
            public int? Audio_conf { get; set; }
            public string Audio_conf_value { get; set; }
            public int? Video_conf { get; set; }
            public string Video_conf_value { get; set; }
            public double? Calling_UK_mins { get; set; }
            public double? freeph_non_geo_loc_min { get; set; }
            public int? Unlimited_inbound_conf { get; set; }
            public Double? Unlimited_inbound_conf_value { get; set; }
            public DateTime? created_date { get; set; }
            public string created_by { get; set; }
            public string UserName { get; set; }
            public int? status { get; set; }
            public string Pc_price_discount { get; set; }
            public int? free_trial_days { get; set; }
            public int? is_reseller { get; set; }
            public int? FT_audio_conf { get; set; }
            public int? FT_audio_conf_value { get; set; }
            public int? FT_video_conf { get; set; }
            public int? FT_video_conf_value { get; set; }
            public int? FT_Unlimted_Intra_Call { get; set; }
            public int? FT_Unlimted_Intra_Call_min { get; set; }
            public int? Free_trial_min { get; set; }
            public double? reseller_discount { get; set; }
            public int? PC_From_Users { get; set; }
            public int? PC_To_Users { get; set; }
            public string Video_conf_meetings { get; set; }
            public string Video_conf_participants { get; set; }
            public string Audio_conf_participants { get; set; }
            public string FT_video_conf_meetings { get; set; }
            public string FT_video_conf_participants { get; set; }
            public string FT_Audio_conf_participants { get; set; }
            public int? Video_conf_duration { get; set; }
            public string plan_show_output { get; set; }
            public string addon_info { get; set; }
            public string MemberType_output { get; set; }
        }
    }
}