﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UTMSupdateFreetrialsettingsinfoModel
    {
        public class UTMSupdateFreetrialsettingsinfoInput
        {
            public int? Free_trial_id { get; set; }
            public int? Free_trial_day { get; set; }
            public string Free_trial_status { get; set; }

        }
        public class UTMSupdateFreetrialsettingsinfoOutput : ErrorMessageModel
        {
            //public string  Free_trial_status { get; set; }
        }
    }
}