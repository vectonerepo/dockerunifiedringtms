﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UTMSGetRoleInfoModel
    {
        public class UTMSGetRoleInfoInput
        {
            public int Pk_role_id { get; set; }
        }
        public class UTMSGetRoleInfoOutput : ErrorMessageModel
        {
            public int Pk_role_id { get; set; }
            public string Role_name { get; set; }
            public DateTime? Role_created_date { get; set; }
            public int? role_status { get; set; }
            public string menu_id { get; set; }
        }
    }
}