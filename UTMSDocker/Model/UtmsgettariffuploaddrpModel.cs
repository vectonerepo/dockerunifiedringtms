﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmsgettariffuploaddrpModel
    {
        public class UtmsgettariffuploaddrpInput
        {
            public int type { get; set; }
        }
        public class UtmsgettariffuploaddrpOutput:ErrorMessageModel
        {
            public int id { get; set; }
            public string value { get; set; }
        }
    }
}