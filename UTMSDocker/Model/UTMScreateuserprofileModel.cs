﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UTMScreateuserprofileModel
    {
        public class UTMScreateuserprofileInput
        {
            public string First_name { get; set; }
            public string Last_name { get; set; }
            public string Username { get; set; }
            public string Password { get; set; }
            public int role_id { get; set; }
            public string last_login_ip { get; set; }
            public int Status { get; set; }
            public string Menu_id { get; set; }
            public int process_Type { get; set; }
            public int user_id { get; set; } 
        }
        public class UTMScreateuserprofileOutput : ErrorMessageModel
        {
          
            //public int errcode { get; set; }
            //public string errmsg { get; set; }
        }
    }
}