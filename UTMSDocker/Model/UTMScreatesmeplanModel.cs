﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace UTMSDocker.Models
{

    public class UTMScreatesmeplanModel
    {
        public class UTMScreatesmeplanInput
        {
            public string  created_by { get; set; }
            public int status { get; set; }
            public int Audio_conf { get; set; }
            public string Audio_conf_value { get; set; }
            public int Video_conf { get; set; }
            public string Video_conf_value { get; set; }
            public double? Calling_UK_mins { get; set; }
            public int User_Id { get; set; }
            public int Reseller_ID { get; set; }
            public int Product_Id { get; set; }
            public int Pc_From_Users { get; set; }
            public int Pc_To_Users { get; set; }
            public double? PC_Price { get; set; }
            public string features { get; set; }
            public int Process_Type { get; set; }
            public int National_min { get; set; }
            public int Geo_Min { get; set; }
            public string Plan_Name { get; set; }
            public int Request_ID { get; set; }
            public int Unlimted_Intra_Call { get; set; }
            public int is_free_trial { get; set; }
            public double? monthly_price { get; set; }
            public double? yearly_price { get; set; }
            public double? monthly_yearly_price { get; set; }
            public double? yearly_discount { get; set; }
            public double? monthly_yearly_discount { get; set; }
            public int most_popular { get; set; }
            public double? freeph_non_geo_loc_min { get; set; }
            public int Unlimited_inbound_conf { get; set; }
            public double? Unlimited_inbound_conf_value { get; set; }
            public int free_trial_days { get; set; }
            public int is_reseller { get; set; }
            public int FT_audio_conf { get; set; }
            public int FT_audio_conf_value { get; set; }
            public int FT_video_conf { get; set; }
            public int FT_video_conf_value { get; set; }
            public int FT_Unlimted_Intra_Call { get; set; }
            public int FT_Unlimted_Intra_Call_min { get; set; }
            public int Free_trial_min { get; set; }
            public int reseller_discount_flag { get; set; }
            public string reseller_discount_xml { get; set; }
            public double? two_year_monthly_price { get; set; }
            public double? three_year_monthly_price { get; set; }
            public double? two_year_monthly_discount { get; set; }
            public double? three_year_monthly_discount { get; set; }
            public string User_Range_xml { get; set; }
            public string plan_addon_price_xml { get; set; }
            public string Video_conf_meetings { get; set; }
            public string Video_conf_participants { get; set; }
            public string Audio_conf_participants { get; set; }
            public string FT_video_conf_meetings { get; set; }
            public string FT_video_conf_participants { get; set; }
            public string FT_Audio_conf_participants { get; set; }
            public string Feature_xml { get; set; }

            public int Video_conf_duration { get; set; }


        }
        public class UTMScreatesmeplanOutput : ErrorMessageModel
        {

        }
    }
}