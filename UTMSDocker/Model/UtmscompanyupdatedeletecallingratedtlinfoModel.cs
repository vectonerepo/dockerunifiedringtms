﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmscompanyupdatedeletecallingratedtlinfoModel
    {
        public class UtmscompanyupdatedeletecallingratedtlinfoInput
        {
            public int processtype { get; set; }
            public int rate_id { get; set; }
            public string type { get; set; }
            public string prefix { get; set; }
            public string call_rate { get; set; }
            public int status { get; set; }
            public int calling_id { get; set; }
            public string destination { get; set; }
        }
        public class UtmscompanyupdatedeletecallingratedtlinfoOutput:ErrorMessageModel
        {
            
        }
    }
}