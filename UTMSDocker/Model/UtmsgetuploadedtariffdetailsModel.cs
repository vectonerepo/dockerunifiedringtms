﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmsgetuploadedtariffdetailsModel
    {
        public class UtmsgetuploadedtariffdetailsInput
        {
        }
        public class UtmsgetuploadedtariffdetailsOutput:ErrorMessageModel
        {
            public string trffclass { get; set; }
            public int access_type { get; set; }
            public string access_type_name { get; set; }
            public string plan_name { get; set; }
        }
    }
}