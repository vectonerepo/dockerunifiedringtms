﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmsgetplanaddonbyuserModel
    {
        public class UtmsgetplanaddonbyuserInput
        {
            public int pc_planid { get; set; }
        }
        public class UtmsgetplanaddonbyuserOutput :ErrorMessageModel
        {
            public int? pc_id { get; set; }
            public int? pc_planid { get; set; }
            public int? pc_addon_id { get; set; }
            public string pc_addon_feature_name { get; set; }
            public int? pc_addon_flag { get; set; }
            public double? pc_addon_price { get; set; }
            public DateTime? create_date { get; set; }
            public string create_by { get; set; }
        }
    }
}