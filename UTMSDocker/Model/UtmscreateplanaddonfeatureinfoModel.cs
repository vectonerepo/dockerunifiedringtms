﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmscreateplanaddonfeatureinfoModel
    {

        public class UtmscreateplanaddonfeatureinfoInput
        {
            public int addon_avail_at { get; set; }
            public string addon_feature_Name { get; set; }
            public string addon_feature_desc { get; set; }
            public int addon_status { get; set; }
            public int process_type { get; set; }
            public int addon_id { get; set; }
            public double? addon_price { get; set; }
            public int is_third_party { get; set; }
            public double? Company_addon_price { get; set; }
            public string groupname { get; set; }
            public int no_group_allowed { get; set; }
            public double? group_price { get; set; }
            public int max_user_in_group { get; set; }
            public string members_type { get; set; }
            public int group_within_plan { get; set; }
            public int Feature_Type { get; set; }
            public int Feature_limit { get; set; }
            public string Tariff { get; set; }
            public int category_id { get; set; }
            public string integrate_name { get; set; }
            public string limit_set_text { get; set; }
            public double? limit_set_value { get; set; }
           
        }

        public class UtmscreateplanaddonfeatureinfoOutput:ErrorMessageModel
        {
            
        }
    }
}