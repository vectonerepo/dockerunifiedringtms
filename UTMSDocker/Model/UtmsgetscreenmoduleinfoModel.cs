﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmsgetscreenmoduleinfoModel
    {
        public class UtmsgetscreenmoduleinfoInput
        {
            public int? Module_id { get; set; }
        }
        public class UtmsgetscreenmoduleinfoOutput:ErrorMessageModel
        {
            public int? Module_Id { get; set; }
            public string Module_name { get; set; }
            public int? Status { get; set; }
            public string Url { get; set; }
        }
    }
}