﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UTMSgetplanpricebyuserRangeModel
    {
        public class UTMSgetplanpricebyuserRangeInput
        {
            public int? plan_id { get; set; }

        }
        public class UTMSgetplanpricebyuserRangeOutput : ErrorMessageModel
        {
            public int? PC_Plan { get; set; }
            public int? Duration { get; set; }
            public int? From_User { get; set; }
            public int? To_User { get; set; }
            public double? price { get; set; }
            public string type_desc { get; set; }
            public int? plan_dur_flag_id { get; set; }
        }
    }
}