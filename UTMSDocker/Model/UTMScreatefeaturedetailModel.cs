﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UTMScreatefeaturedetailModel
    {
        public class UTMScreatefeaturedetailInput
        {
            
            public string feature_name { get; set; }
            public string feature_desc { get; set; }
            public string code { get; set; }
            public int? Feature_hdr { get; set; }
            public int? process_type { get; set; }
            public string last_upd_by { get; set; }
            public int? Feature_idx { get; set; }
            public int? Category_id { get; set; }
            public int? status { get; set; }
            public int? is_freetrial { get; set; }
            public  double Feature_price { get; set; }
        }
        public class UTMScreatefeaturedetailOutput : ErrorMessageModel
        {
         
        }
    }
}