﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UTMSgetCategoryinfoModel
    {
        public class UTMSgetCategoryinfoInput
        {
            public int? Category_id { get; set; }

        }
        public class UTMSgetCategoryinfoOutput : ErrorMessageModel
        {
            public int? Category_id { get; set; }
            public string Category_Name { get; set; }
            public int category_status { get; set; }
            public int? category_position { get; set; }
        }
    }
}