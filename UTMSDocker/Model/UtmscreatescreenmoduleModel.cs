﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmscreatescreenmoduleModel
    {
        public class UtmscreatescreenmoduleInput
        {
            public string Module_name { get; set; }
            public string Url { get; set; }
            public int Status { get; set; }
            public int Module_Id { get; set; }
            public int processtype { get; set; }
        }
        public class UtmscreatescreenmoduleOutput:ErrorMessageModel
        {
        }
    }
}