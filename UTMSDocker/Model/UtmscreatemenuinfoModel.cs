﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmscreatemenuinfoModel
    {

        public class UtmscreatemenuinfoInput
        {
            public string Menu_Name { get; set; }
            public string Menu_link { get; set; }
            public int? process_type { get; set; }
            public int? Menu_Id { get; set; }
        }
        public class UtmscreatemenuinfoOutput : ErrorMessageModel
        {

        
        }
    }
}