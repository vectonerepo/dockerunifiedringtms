﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmsportalcreatebundleplanModel
    {
        public class UtmsportalcreatebundleplanInput
        {
            public string sitecode { get; set; }
            public string bundle_name { get; set; }
            public string currency { get; set; }
            public double? price { get; set; }
            public int tariffclass_groupid { get; set; }
            public int renewal_delay { get; set; }
            public int renewal_mode { get; set; }
            public int pro_rata { get; set; }
            public double? national_min { get; set; }
            public double? international_min { get; set; }
            public int bundle_type { get; set; }
            public int reseller_flag { get; set; }
        }
        public class UtmsportalcreatebundleplanOutput:ErrorMessageModel
        {
            public string sitecode { get; set; }
            public string currency { get; set; }
            public double? price { get; set; }
            public int? tariffclass_groupid { get; set; }
            public int? renewal_delay { get; set; }
            public int? renewal_mode { get; set; }
            public int? pro_rata { get; set; }
            public double? national_min { get; set; }
            public double? international_min { get; set; }
            public int? bundleid { get; set; }
            public string bundle_name { get; set; }
        }
    }
}