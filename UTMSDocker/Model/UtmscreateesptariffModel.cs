﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmscreateesptariffModel
    {
        public class UtmscreateesptariffInput
        {
            public int plan_id { get; set; }
            public string tariff { get; set; }
            public int tariff_type { get; set; }
            public string processby { get; set; }
            public int processtype { get; set; }
            public int id { get; set; }
        }
        public class UtmscreateesptariffOutput:ErrorMessageModel
        {

        }
    }
}