﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UTMSgetactivityloginfoModel
    {

        public class UTMSgetactivityloginfoInput
        {         
            public string From_date { get; set; }
            public string  to_date { get; set; }
        }
        public class UTMSgetactivityloginfoOutput : ErrorMessageModel
        {
            public int? created_by { get; set; }
            public string  Name { get; set; }
            public string module_name { get; set; }
            public DateTime? create_date { get; set; }
            public string  comments { get; set; }
        }
    }
}