﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UTMSupdatepricesettingsinfoModel
    {

        public class UTMSupdatepricesettingsinfoInput
        {
            public int? pricing_id { get; set; }
            public string  pricing_status { get; set; }  // Added on 29/11//2019
        }
        public class UTMSupdatepricesettingsinfoOutput : ErrorMessageModel
        {

        }
    }
}