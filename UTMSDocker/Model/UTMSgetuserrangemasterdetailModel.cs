﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UTMSgetuserrangemasterdetailModel
    {
        public class UTMSgetuserrangemasterdetailInput
        {
            public int Range_typeid { get; set; }
            public int plan_id { get; set; }
        }
        public class UTMSgetuserrangemasterdetailOutput:ErrorMessageModel
        {
            public int? Range_typeid { get; set; }
            public int? From_user_Range { get; set; }
            public int? To_user_Range { get; set; }
            public int? min_user { get; set; }
        }
        
    }
}