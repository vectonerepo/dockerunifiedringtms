﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UTMSgetdiscountpercentagesettingsinfoModel
    {

        public class UTMSgetdiscountpercentagesettingsinfoOutput : ErrorMessageModel
        {
            public int? discount_percentage_id { get; set; }
            public string discount_percentage_status { get; set; }
            public double? discount_percentage { get; set; }
            public int? max_no_of_user { get; set; }
        }
        
        
    }
}