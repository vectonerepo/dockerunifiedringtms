﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmscompanygetcallingtypeinfoModel
    {


        public class UtmscompanygetcallingtypeinfoInput
        {

        }

        public class UtmscompanygetcallingtypeinfoOutput:ErrorMessageModel
        {
            public int? calling_id { get; set; }
            public string calling_type { get; set; }
            public int? status { get; set; }
        }
    }
}