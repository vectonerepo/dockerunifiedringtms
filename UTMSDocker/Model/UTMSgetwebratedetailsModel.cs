﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UTMSgetwebratedetailsModel
    {
        public class UTMSgetwebratedetailsInput
        {           
            public string callingtype { get; set; }
        }
        public class UTMSgetwebratedetailsOutput : ErrorMessageModel
        {
            public int id { get; set; }
            public string calling_type { get; set; }
            public string country { get; set; }
            public string prefix { get; set; }
            public string fixed_rate { get; set; }
            public string mobile_rate { get; set; }
            public string Type { get; set; }
            public string rate { get; set; }

        }
    }
}