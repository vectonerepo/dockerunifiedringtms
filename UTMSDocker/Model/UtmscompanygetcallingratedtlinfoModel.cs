﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmscompanygetcallingratedtlinfoModel
    {
        public class UtmscompanygetcallingratedtlinfoInput
        {
            public int id { get; set; }
            public int calling_id { get; set; }
        }
        public class UtmscompanygetcallingratedtlinfoOutput:ErrorMessageModel
        {
            public int? id { get; set; }
            public int? calling_id { get; set; }
            public string destination { get; set; }
            public string  type { get; set; }
            public string  prefix { get; set; }
            public string  call_rate { get; set; }
            public int? status { get; set; }
            public string calling_type { get; set; }
        }

    }
}