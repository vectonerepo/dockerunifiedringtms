﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UrtariffgettariffdetailsModel
    {
        public class UrtariffgettariffdetailsInput
        {
            public int id { get; set; }
            public int planid { get; set; }
        }
        public class UrtariffgettariffdetailsOutput:ErrorMessageModel
        {
            public int Plan_id { get; set; }
            public string Pln_Name { get; set; }
            public string trffclass { get; set; }
            public int trf_type { get; set; }
            public string plan_status { get; set; }
            public int? id { get; set; }
        }
    }
}