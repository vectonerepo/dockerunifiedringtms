﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmsgetplanaddonfeatureinfoModel
    {
        public class UtmsgetplanaddonfeatureinfoInput
        {
            public int pc_addon_id { get; set; }
            public int pc_addon_available_level { get; set; }
            public int category_id { get; set; }
        }
        public class UtmsgetplanaddonfeatureinfoOutput:ErrorMessageModel
        {
            public int? pc_addon_id { get; set; }
            public int? pc_addon_available_level { get; set; }
            public string pc_addon_feature_name { get; set; }
            public string pc_addon_feature_desc { get; set; }
            public int? pc_addon_status { get; set; }
            public double? pc_addon_price { get; set; }
            public int? is_third_party { get; set; }
            public double? Company_addon_price { get; set; }

            public string group_name { get; set; }
            public int? max_user_group { get; set; }
            public int? no_group_allowed { get; set; }
            public double? group_price { get; set; }
            public int? group_within_plan { get; set; }
            public string grp_member_type { get; set; }

            public int Feature_Type { get; set; }
            public int Feature_limit { get; set; }
            public string tariff { get; set; }
            public int? category_id { get; set; }
            public string Integration_name { get; set; }
            public string limit_set_text { get; set; }
            public double? limit_set_value { get; set; }
            public string category_name { get; set; }  //aded on 27/07/2021
            
        }
    }
}