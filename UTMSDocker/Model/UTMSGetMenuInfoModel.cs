﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UTMSGetMenuInfoModel
    {
        public class UTMSGetMenuInfoInput
        {
            public int? Menu_Id { get; set; }
        }
        public class UTMSGetMenuInfoOutput : ErrorMessageModel
        {
            public int? Menu_Id { get; set; }
            public string Menu_Name { get; set; }
            public string Menu_link { get; set; }
        }

    }
}