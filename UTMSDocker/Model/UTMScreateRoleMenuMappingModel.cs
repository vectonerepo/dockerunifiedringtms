﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UTMScreateRoleMenuMappingModel 
    {
        public class UTMScreateRoleMenuMappingInput
        {
            public string Role_Name { get; set; }
            public string menu_id { get; set; }
            public int status { get; set; }
            public int process_type { get; set; }
            public int Role_ID { get; set; }
        }
        public class UTMScreateRoleMenuMappingOutput:ErrorMessageModel
        {
            public DateTime? Role_created_date { get; set; }
            public int role_status { get; set; }
        }
    }
}