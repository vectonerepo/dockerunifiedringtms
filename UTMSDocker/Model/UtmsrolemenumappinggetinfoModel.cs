﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmsrolemenumappinggetinfoModel
    {
        public class UtmsrolemenumappinggetinfoInput
        {
            public int Role_ID { get; set; }
            public int menu_id { get; set; }

        }
        public class UtmsrolemenumappinggetinfoOutput : ErrorMessageModel
        {

        }
    }
}