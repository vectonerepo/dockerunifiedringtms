﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmsgetactiveplaninfoModel
    {
        public class UtmsgetactiveplaninfoOutput:ErrorMessageModel
        {
            public int? Pln_Idx { get; set; }
            public string Pln_Name { get; set; }
        }
    }
}