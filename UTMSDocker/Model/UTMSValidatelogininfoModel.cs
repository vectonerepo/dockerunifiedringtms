﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UTMSValidatelogininfoModel
    {
        public class UTMSValidatelogininfoInput
        {
            public string Username { get; set; }
            public string Password { get; set; }
            public string Last_login_Ip { get; set; }
        }
        public class UTMSValidatelogininfoOutput : ErrorMessageModel
        {
            public int role_id { get; set; }
            public int UserId { get; set; }
        }
    }
}