﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmsgetwebratescountryprefixinfoModel
    {
        public class UtmsgetwebratescountryprefixinfoInput
        {
            public string callingtype { get; set; }
            public string type { get; set; }
            public string country { get; set; }
        }
        public class UtmsgetwebratescountryprefixinfoOutput:ErrorMessageModel
        {
            public string calling_type { get; set; }
            public string country { get; set; }
            public string prefix { get; set; }
            public int? id { get; set; }
        }
    }
}