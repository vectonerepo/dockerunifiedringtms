﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmsuplaodesptariffratesModel
    {

        public class UtmsuplaodesptariffratesInput
        {
            public string login { get; set; }
            public string tariffclass { get; set; }
            public int access_type { get; set; }
            public DateTime? effectivedate { get; set; }
            public string filename { get; set; }
            public string tempfilename { get; set; }
            public int upload_type { get; set; }
        }
        public class UtmsuplaodesptariffratesOutput:ErrorMessageModel
        {

        }
    }
}