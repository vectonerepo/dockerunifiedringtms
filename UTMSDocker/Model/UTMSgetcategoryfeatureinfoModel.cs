﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UTMSgetcategoryfeatureinfoModel
    {
        public class UTMSgetcategoryfeatureinfoInput
        {
            public int Category_id { get; set; }
        }
        public class UTMSgetcategoryfeatureinfoOutput : ErrorMessageModel
        {
            public int PF_Idx { get; set; }
            public string PF_Name { get; set; }
            public string PF_Description { get; set; }
            public string code { get; set; }
            public string  PF_category { get; set; }
            public string  last_upd_by { get; set; }
            public int Category_id { get; set; }
            public int PF_status { get; set; }
        }
    }
}