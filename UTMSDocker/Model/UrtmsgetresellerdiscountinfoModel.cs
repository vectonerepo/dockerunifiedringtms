﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UrtmsgetresellerdiscountinfoModel
    {
        public class UrtmsgetresellerdiscountinfoInput
        {
            public int product_id { get; set; }
        }
        public class UrtmsgetresellerdiscountinfoOutput : ErrorMessageModel
        {
            public int? user_range_from { get; set; }
            public int? user_range_to { get; set; }
            public double? discount { get; set; }
        }
    }
}