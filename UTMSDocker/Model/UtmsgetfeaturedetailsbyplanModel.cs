﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmsgetfeaturedetailsbyplanModel
    {

        public class UtmsgetfeaturedetailsbyplanInput
        {
            //public string plan_id { get; set; }
            public int plan_id { get; set; }
        }
        public class UtmsgetfeaturedetailsbyplanOutput:ErrorMessageModel
        {
            public int PFD_Feature { get; set; }
            //public double price { get; set; }
            public string FT_Keyword { get; set; }
        }
    }
}