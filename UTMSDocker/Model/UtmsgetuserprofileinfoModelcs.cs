﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Models
{
    public class UtmsgetuserprofileinfoModelcs
    {
        public class UtmsgetuserprofileinfoInput
        {
            public int? User_ID { get; set; }
        }
        public class UtmsgetuserprofileinfoOutput:ErrorMessageModel
        {
            public int? Userid { get; set; }
            public string First_name { get; set; } 
            public string Last_name { get; set; }
            public string Username { get; set; }
            public string Password { get; set; }
            public string Role_name { get; set; }
            public int? Fk_role_Id { get; set; }
            public string last_login_ip { get; set; }
            public string User_status { get; set; }
        }
    }
}