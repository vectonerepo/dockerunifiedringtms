﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UTMSGetRoleInfoModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UTMSGetRoleInfoController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UTMSGetRoleInfoController> _logger;

        public UTMSGetRoleInfoController(ILogger<UTMSGetRoleInfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UTMSGetRoleInfoOutput>> CreateAsync(UTMSGetRoleInfoInput req)
        {
            List<UTMSGetRoleInfoOutput> result = new List<UTMSGetRoleInfoOutput>();
            UTMSGetRoleInfoInput objSignInInput = new UTMSGetRoleInfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UTMSGetRoleInfoDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UTMSGetRoleInfoOutput outputobj = new UTMSGetRoleInfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
