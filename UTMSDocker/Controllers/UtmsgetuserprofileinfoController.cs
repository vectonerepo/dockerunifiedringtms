﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using UTMSDocker.DB;
using static UTMSDocker.Models.UtmsgetuserprofileinfoModelcs;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UtmsgetuserprofileinfoController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UtmsgetuserprofileinfoController> _logger;

        public UtmsgetuserprofileinfoController(ILogger<UtmsgetuserprofileinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UtmsgetuserprofileinfoOutput>> CreateAsync(UtmsgetuserprofileinfoInput req)
        {
            List<UtmsgetuserprofileinfoOutput> result = new List<UtmsgetuserprofileinfoOutput>();
            UtmsgetuserprofileinfoInput objSignInInput = new UtmsgetuserprofileinfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UtmsgetuserprofileinfoDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UtmsgetuserprofileinfoOutput outputobj = new UtmsgetuserprofileinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
