﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UtmsgetsmeplandetailsModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UtmsgetsmeplandetailsController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UtmsgetsmeplandetailsController> _logger;

        public UtmsgetsmeplandetailsController(ILogger<UtmsgetsmeplandetailsController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UtmsgetsmeplandetailsOutput>> CreateAsync(UtmsgetsmeplandetailsInput req)
        {
            List<UtmsgetsmeplandetailsOutput> result = new List<UtmsgetsmeplandetailsOutput>();
            UtmsgetsmeplandetailsInput objSignInInput = new UtmsgetsmeplandetailsInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UtmsgetsmeplandetailsDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UtmsgetsmeplandetailsOutput outputobj = new UtmsgetsmeplandetailsOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
