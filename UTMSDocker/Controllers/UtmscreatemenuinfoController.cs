﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NLog; using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTMSDocker.DB;
using static UTMSDocker.Models.UtmscreatemenuinfoModel;

namespace UTMSDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UtmscreatemenuinfoController : ControllerBase
    {
         
        public IConfiguration _Config { get; }  private readonly ILogger<UtmscreatemenuinfoController> _logger;

        public UtmscreatemenuinfoController(ILogger<UtmscreatemenuinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UtmscreatemenuinfoOutput>> CreateAsync(UtmscreatemenuinfoInput req)
        {
            List<UtmscreatemenuinfoOutput> result = new List<UtmscreatemenuinfoOutput>();
            UtmscreatemenuinfoInput objSignInInput = new UtmscreatemenuinfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UtmscreatemenuinfoDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UtmscreatemenuinfoOutput outputobj = new UtmscreatemenuinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}
