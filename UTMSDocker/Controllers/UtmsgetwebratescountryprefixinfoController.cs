﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UtmsgetwebratescountryprefixinfoModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UtmsgetwebratescountryprefixinfoController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UtmsgetwebratescountryprefixinfoController> _logger;

        public UtmsgetwebratescountryprefixinfoController(ILogger<UtmsgetwebratescountryprefixinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UtmsgetwebratescountryprefixinfoOutput>> CreateAsync(UtmsgetwebratescountryprefixinfoInput req)
        {
            List<UtmsgetwebratescountryprefixinfoOutput> result = new List<UtmsgetwebratescountryprefixinfoOutput>();
            UtmsgetwebratescountryprefixinfoInput objSignInInput = new UtmsgetwebratescountryprefixinfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UtmsgetwebratescountryprefixinfoDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UtmsgetwebratescountryprefixinfoOutput outputobj = new UtmsgetwebratescountryprefixinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
