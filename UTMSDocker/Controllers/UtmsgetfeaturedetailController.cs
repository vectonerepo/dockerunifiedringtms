﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using UTMSDocker.DB;
using static UTMSDocker.Models.UtmsgetfeaturedetailModel;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UtmsgetfeaturedetailController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UtmsgetfeaturedetailController> _logger;

        public UtmsgetfeaturedetailController(ILogger<UtmsgetfeaturedetailController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UtmsgetfeaturedetailOutput>> CreateAsync(UtmsgetfeaturedetailInput req)
        {
            List<UtmsgetfeaturedetailOutput> result = new List<UtmsgetfeaturedetailOutput>();
            UtmsgetfeaturedetailInput objSignInInput = new UtmsgetfeaturedetailInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UtmsgetfeaturedetailDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UtmsgetfeaturedetailOutput outputobj = new UtmsgetfeaturedetailOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
