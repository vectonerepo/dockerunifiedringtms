﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UTMSgetwebratedetailsModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UTMSgetwebratedetailsController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UTMSgetwebratedetailsController> _logger;

        public UTMSgetwebratedetailsController(ILogger<UTMSgetwebratedetailsController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UTMSgetwebratedetailsOutput>> CreateAsync(UTMSgetwebratedetailsInput req)
        {
            List<UTMSgetwebratedetailsOutput> result = new List<UTMSgetwebratedetailsOutput>();
            UTMSgetwebratedetailsInput objSignInInput = new UTMSgetwebratedetailsInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UTMSgetwebratedetailsDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UTMSgetwebratedetailsOutput outputobj = new UTMSgetwebratedetailsOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
