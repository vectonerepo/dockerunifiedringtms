﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NLog; using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTMSDocker.DB;
using static UTMSDocker.Models.UtmscompanygetcallingtypeinfoModel;

namespace UTMSDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UtmscompanygetcallingtypeinfoController : ControllerBase
    {
         
        public IConfiguration _Config { get; }  private readonly ILogger<UtmscompanygetcallingtypeinfoController> _logger;

        public UtmscompanygetcallingtypeinfoController(ILogger<UtmscompanygetcallingtypeinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<UtmscompanygetcallingtypeinfoOutput>> CreateAsync()
        {
            List<UtmscompanygetcallingtypeinfoOutput> result = new List<UtmscompanygetcallingtypeinfoOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UtmscompanygetcallingtypeinfoDB.CallDB(_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UtmscompanygetcallingtypeinfoOutput outputobj = new UtmscompanygetcallingtypeinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }

    }
}
