﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UTMScreatesmeplanModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UTMScreatesmeplanController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UTMScreatesmeplanController> _logger;

        public UTMScreatesmeplanController(ILogger<UTMScreatesmeplanController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UTMScreatesmeplanOutput>> CreateAsync(UTMScreatesmeplanInput req)
        {
            _logger.LogInformation("Input : UTMScreatesmeplanController start:" + JsonConvert.SerializeObject(req));
            List<UTMScreatesmeplanOutput> result = new List<UTMScreatesmeplanOutput>();
            UTMScreatesmeplanInput objSignInInput = new UTMScreatesmeplanInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UTMScreatesmeplanDB.CallDB(req, _Config,_logger);
                _logger.LogInformation("Input : UTMScreatesmeplanController result:" + JsonConvert.SerializeObject(result));
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UTMScreatesmeplanOutput outputobj = new UTMScreatesmeplanOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);                
                return Ok(outputobj);
            }
        }
    }


}
