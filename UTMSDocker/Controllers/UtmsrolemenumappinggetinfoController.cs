﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UtmsrolemenumappinggetinfoModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UtmsrolemenumappinggetinfoController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UtmsrolemenumappinggetinfoController> _logger;

        public UtmsrolemenumappinggetinfoController(ILogger<UtmsrolemenumappinggetinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UtmsrolemenumappinggetinfoOutput>> CreateAsync(UtmsrolemenumappinggetinfoInput req)
        {
            List<UtmsrolemenumappinggetinfoOutput> result = new List<UtmsrolemenumappinggetinfoOutput>();
            UtmsrolemenumappinggetinfoInput objSignInInput = new UtmsrolemenumappinggetinfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UtmsrolemenumappinggetinfoDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UtmsrolemenumappinggetinfoOutput outputobj = new UtmsrolemenumappinggetinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
