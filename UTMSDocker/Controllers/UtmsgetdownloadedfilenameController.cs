﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UtmsgetdownloadedfilenameModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UtmsgetdownloadedfilenameController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UtmsgetdownloadedfilenameController> _logger;

        public UtmsgetdownloadedfilenameController(ILogger<UtmsgetdownloadedfilenameController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UtmsgetdownloadedfilenameOutput>> CreateAsync(UtmsgetdownloadedfilenameInput req)
        {
            List<UtmsgetdownloadedfilenameOutput> result = new List<UtmsgetdownloadedfilenameOutput>();
            UtmsgetdownloadedfilenameInput objSignInInput = new UtmsgetdownloadedfilenameInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UtmsgetdownloadedfilenameDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UtmsgetdownloadedfilenameOutput outputobj = new UtmsgetdownloadedfilenameOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
