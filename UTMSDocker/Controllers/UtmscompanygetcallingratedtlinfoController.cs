﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NLog; using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTMSDocker.DB;
using static UTMSDocker.Models.UtmscompanygetcallingratedtlinfoModel;

namespace UTMSDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UtmscompanygetcallingratedtlinfoController : ControllerBase
    {
         

        public IConfiguration _Config { get; }  private readonly ILogger<UtmscompanygetcallingratedtlinfoController> _logger;

        public UtmscompanygetcallingratedtlinfoController(ILogger<UtmscompanygetcallingratedtlinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UtmscompanygetcallingratedtlinfoOutput>> CreateAsync(UtmscompanygetcallingratedtlinfoInput req)
        {
            List<UtmscompanygetcallingratedtlinfoOutput> result = new List<UtmscompanygetcallingratedtlinfoOutput>();
            UtmscompanygetcallingratedtlinfoInput objSignInInput = new UtmscompanygetcallingratedtlinfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UtmscompanygetcallingratedtlinfoDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {
                UtmscompanygetcallingratedtlinfoOutput outputobj = new UtmscompanygetcallingratedtlinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}
