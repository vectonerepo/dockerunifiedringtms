﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using UTMSDocker.DB;
using static UTMSDocker.Models.UtmscreatescreenmoduleModel;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UtmscreatescreenmoduleController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UtmscreatescreenmoduleController> _logger;

        public UtmscreatescreenmoduleController(ILogger<UtmscreatescreenmoduleController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UtmscreatescreenmoduleOutput>> CreateAsync(UtmscreatescreenmoduleInput req)
        {
            List<UtmscreatescreenmoduleOutput> result = new List<UtmscreatescreenmoduleOutput>();
            UtmscreatescreenmoduleInput objSignInInput = new UtmscreatescreenmoduleInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UtmscreatescreenmoduleDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UtmscreatescreenmoduleOutput outputobj = new UtmscreatescreenmoduleOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
