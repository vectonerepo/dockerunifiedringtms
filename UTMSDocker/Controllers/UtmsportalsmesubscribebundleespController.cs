﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UtmsportalsmesubscribebundleespModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UtmsportalsmesubscribebundleespController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UtmsportalsmesubscribebundleespController> _logger;

        public UtmsportalsmesubscribebundleespController(ILogger<UtmsportalsmesubscribebundleespController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UtmsportalsmesubscribebundleespOutput>> CreateAsync(UtmsportalsmesubscribebundleespInput req)
        {
            List<UtmsportalsmesubscribebundleespOutput> result = new List<UtmsportalsmesubscribebundleespOutput>();
            UtmsportalsmesubscribebundleespInput objSignInInput = new UtmsportalsmesubscribebundleespInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UtmsportalsmesubscribebundleespDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UtmsportalsmesubscribebundleespOutput outputobj = new UtmsportalsmesubscribebundleespOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
