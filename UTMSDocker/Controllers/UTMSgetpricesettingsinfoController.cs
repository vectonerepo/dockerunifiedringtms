﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NLog; using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTMSDocker.DB;
using static UTMSDocker.Models.UTMSgetpricesettingsinfoModel;

namespace UTMSDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UTMSgetpricesettingsinfoController : ControllerBase
    {
         
        public IConfiguration _Config { get; }  private readonly ILogger<UTMSgetpricesettingsinfoController> _logger;

        public UTMSgetpricesettingsinfoController(ILogger<UTMSgetpricesettingsinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<UTMSgetpricesettingsinfoOutput>> CreateAsync()
        {
            List<UTMSgetpricesettingsinfoOutput> result = new List<UTMSgetpricesettingsinfoOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UTMSgetpricesettingsinfoDB.CallDB(_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UTMSgetpricesettingsinfoOutput outputobj = new UTMSgetpricesettingsinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}
