﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UTMSupdatediscountpercentagesettingsinfoModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UTMSupdatediscountpercentagesettingsinfoController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UTMSupdatediscountpercentagesettingsinfoController> _logger;

        public UTMSupdatediscountpercentagesettingsinfoController(ILogger<UTMSupdatediscountpercentagesettingsinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UTMSupdatediscountpercentagesettingsinfoOutput>> CreateAsync(UTMSupdatediscountpercentagesettingsinfoInput req)
        {
            List<UTMSupdatediscountpercentagesettingsinfoOutput> result = new List<UTMSupdatediscountpercentagesettingsinfoOutput>();
            UTMSupdatediscountpercentagesettingsinfoInput objSignInInput = new UTMSupdatediscountpercentagesettingsinfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UTMSupdatediscountpercentagesettingsinfoDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UTMSupdatediscountpercentagesettingsinfoOutput outputobj = new UTMSupdatediscountpercentagesettingsinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
