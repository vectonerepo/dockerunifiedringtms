﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UtmsuplaodesptariffratesModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UtmsuplaodesptariffratesController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UtmsuplaodesptariffratesController> _logger;

        public UtmsuplaodesptariffratesController(ILogger<UtmsuplaodesptariffratesController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UtmsuplaodesptariffratesOutput>> CreateAsync(UtmsuplaodesptariffratesInput req)
        {
            List<UtmsuplaodesptariffratesOutput> result = new List<UtmsuplaodesptariffratesOutput>();
            UtmsuplaodesptariffratesInput objSignInInput = new UtmsuplaodesptariffratesInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UtmsuplaodesptariffratesDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UtmsuplaodesptariffratesOutput outputobj = new UtmsuplaodesptariffratesOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
