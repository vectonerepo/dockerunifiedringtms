﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NLog; using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTMSDocker.DB;
using static UTMSDocker.Models.UtmsgetuploadedtariffdetailsModel;

namespace UTMSDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UtmsgetuploadedtariffdetailsController : ControllerBase
    {
         
        public IConfiguration _Config { get; }  private readonly ILogger<UtmsgetuploadedtariffdetailsController> _logger;

        public UtmsgetuploadedtariffdetailsController(ILogger<UtmsgetuploadedtariffdetailsController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<UtmsgetuploadedtariffdetailsOutput>> CreateAsync()
        {
            List<UtmsgetuploadedtariffdetailsOutput> result = new List<UtmsgetuploadedtariffdetailsOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UtmsgetuploadedtariffdetailsDB.CallDB(_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UtmsgetuploadedtariffdetailsOutput outputobj = new UtmsgetuploadedtariffdetailsOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}
