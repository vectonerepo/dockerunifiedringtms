﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UtmsportalcreatebundleplanModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UtmsportalcreatebundleplanController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UtmsportalcreatebundleplanController> _logger;

        public UtmsportalcreatebundleplanController(ILogger<UtmsportalcreatebundleplanController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UtmsportalcreatebundleplanOutput>> CreateAsync(UtmsportalcreatebundleplanInput req)
        {
            List<UtmsportalcreatebundleplanOutput> result = new List<UtmsportalcreatebundleplanOutput>();
            UtmsportalcreatebundleplanInput objSignInInput = new UtmsportalcreatebundleplanInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UtmsportalcreatebundleplanDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UtmsportalcreatebundleplanOutput outputobj = new UtmsportalcreatebundleplanOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
