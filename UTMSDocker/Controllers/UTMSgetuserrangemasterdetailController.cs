﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UTMSgetuserrangemasterdetailModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UTMSgetuserrangemasterdetailController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UTMSgetuserrangemasterdetailController> _logger;

        public UTMSgetuserrangemasterdetailController(ILogger<UTMSgetuserrangemasterdetailController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UTMSgetuserrangemasterdetailOutput>> CreateAsync(UTMSgetuserrangemasterdetailInput req)
        {
            List<UTMSgetuserrangemasterdetailOutput> result = new List<UTMSgetuserrangemasterdetailOutput>();
            UTMSgetuserrangemasterdetailInput objSignInInput = new UTMSgetuserrangemasterdetailInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UTMSgetuserrangemasterdetailDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UTMSgetuserrangemasterdetailOutput outputobj = new UTMSgetuserrangemasterdetailOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
