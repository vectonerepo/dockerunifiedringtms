﻿using UTMSDocker.Auth;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace UTMSDocker.Auth
{
   
    public class JwtAuthManager
    { 
        private const string SecretKey = "F7LVJ6N4ds9SL0D/u1ShRdtwQ4pCWLTv3ChNDnlnFQPeSU7Z2F9t7fdJVCuuwVjdrEM2kDXlEaCSI/6BXRD/BA==";
        //public static void JwtAuthManagerKey()
        //{
        //    var hmac = new HMACSHA256();
        //    SecretKey = Convert.ToBase64String(hmac.Key); 
        //}
        public static string GenerateJWTToken(string username, int expire_in_Minutes = 2)
        { 
            var symmetric_Key = Encoding.ASCII.GetBytes(SecretKey);
            //var symmetric_Key = Encoding.ASCII.GetBytes(SecretKey);
            var token_Handler = new JwtSecurityTokenHandler();

            var now = DateTime.UtcNow;
            var securitytokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                        {
                            new Claim(ClaimTypes.Name, username)
                        }),

                Expires = now.AddMinutes(Convert.ToInt32(expire_in_Minutes)),

                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(symmetric_Key), SecurityAlgorithms.HmacSha256Signature)
            };

            var stoken = token_Handler.CreateToken(securitytokenDescriptor);
            var token = token_Handler.WriteToken(stoken);

            return token;
        }
        public static string ComputeSha256Hash(string rawData)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }
        public static dynamic GetTokenValues(dynamic token)
        { 
            var tokenHandler = new JwtSecurityTokenHandler();
            var jwtToken = tokenHandler.ReadToken(token) as JwtSecurityToken;
            return jwtToken;
        }

        public static ClaimsPrincipal GetPrincipal(string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var jwtToken = tokenHandler.ReadToken(token) as JwtSecurityToken;

                if (jwtToken == null)
                    return null;

                var symmetricKey = Convert.FromBase64String(SecretKey);

                //var validationParameters = new TokenValidationParameters()
                //{
                //    RequireExpirationTime = true,
                //    ValidateIssuer = false,
                //    ValidateAudience = false, 
                //    IssuerSigningKey = new SymmetricSecurityKey(symmetricKey)
                //};
                TokenValidationParameters validationParameters = new TokenValidationParameters()
                {
                    RequireExpirationTime = true,
                    ValidateIssuer = false,
                    ValidateAudience = false, 
                    LifetimeValidator = TokenValidationHandler.LifetimeValidator,
                    IssuerSigningKey = new SymmetricSecurityKey(symmetricKey)
                };

                SecurityToken securityToken;
                var principal = tokenHandler.ValidateToken(token, validationParameters, out securityToken);

                return principal;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        internal class TokenValidationHandler
        {
            public static bool LifetimeValidator(DateTime? notBefore, DateTime? expires, SecurityToken securityToken, TokenValidationParameters validationParameters)
            {
                if (expires != null)
                {
                    if (DateTime.UtcNow < expires) return true;
                }
                return false;
            }
        }
    }
}