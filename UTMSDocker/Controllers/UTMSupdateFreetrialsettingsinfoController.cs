﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UTMSupdateFreetrialsettingsinfoModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UTMSupdateFreetrialsettingsinfoController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UTMSupdateFreetrialsettingsinfoController> _logger;

        public UTMSupdateFreetrialsettingsinfoController(ILogger<UTMSupdateFreetrialsettingsinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UTMSupdateFreetrialsettingsinfoOutput>> CreateAsync(UTMSupdateFreetrialsettingsinfoInput req)
        {
            List<UTMSupdateFreetrialsettingsinfoOutput> result = new List<UTMSupdateFreetrialsettingsinfoOutput>();
            UTMSupdateFreetrialsettingsinfoInput objSignInInput = new UTMSupdateFreetrialsettingsinfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UTMSupdateFreetrialsettingsinfoDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UTMSupdateFreetrialsettingsinfoOutput outputobj = new UTMSupdateFreetrialsettingsinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
