﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UtmsgetresellerdiscountinfoModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UtmsgetresellerdiscountinfoController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UtmsgetresellerdiscountinfoController> _logger;

        public UtmsgetresellerdiscountinfoController(ILogger<UtmsgetresellerdiscountinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UtmsgetresellerdiscountinfoOutput>> CreateAsync(UtmsgetresellerdiscountinfoInput req)
        {
            List<UtmsgetresellerdiscountinfoOutput> result = new List<UtmsgetresellerdiscountinfoOutput>();
            UtmsgetresellerdiscountinfoInput objSignInInput = new UtmsgetresellerdiscountinfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UtmsgetresellerdiscountinfoDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UtmsgetresellerdiscountinfoOutput outputobj = new UtmsgetresellerdiscountinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
