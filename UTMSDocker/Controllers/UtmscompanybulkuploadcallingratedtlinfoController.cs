﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using UTMSDocker.DB;
using static UTMSDocker.Models.UtmscompanybulkuploadcallingratedtlinfoModel;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UtmscompanybulkuploadcallingratedtlinfoController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UtmscompanybulkuploadcallingratedtlinfoController> _logger;

        public UtmscompanybulkuploadcallingratedtlinfoController(ILogger<UtmscompanybulkuploadcallingratedtlinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UtmscompanybulkuploadcallingratedtlinfoOutput>> CreateAsync(UtmscompanybulkuploadcallingratedtlinfoInput req)
        {
            List<UtmscompanybulkuploadcallingratedtlinfoOutput> result = new List<UtmscompanybulkuploadcallingratedtlinfoOutput>();
            UtmscompanybulkuploadcallingratedtlinfoInput objSignInInput = new UtmscompanybulkuploadcallingratedtlinfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UtmscompanybulkuploadcallingratedtlinfoDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UtmscompanybulkuploadcallingratedtlinfoOutput outputobj = new UtmscompanybulkuploadcallingratedtlinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
