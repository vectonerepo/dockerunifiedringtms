﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UrtariffgettariffdetailsModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UrtariffgettariffdetailsController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UrtariffgettariffdetailsController> _logger;  

        public UrtariffgettariffdetailsController(ILogger<UrtariffgettariffdetailsController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UrtariffgettariffdetailsOutput>> CreateAsync(UrtariffgettariffdetailsInput req)
        {
            List<UrtariffgettariffdetailsOutput> result = new List<UrtariffgettariffdetailsOutput>();
            UrtariffgettariffdetailsInput objSignInInput = new UrtariffgettariffdetailsInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrtariffgettariffdetailsDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UrtariffgettariffdetailsOutput outputobj = new UrtariffgettariffdetailsOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
