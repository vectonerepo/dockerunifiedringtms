﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.utmsplanpricinggetnotesinfoModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class utmsplanpricinggetnotesinfoController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<utmsplanpricinggetnotesinfoController> _logger;  

        public utmsplanpricinggetnotesinfoController(ILogger<utmsplanpricinggetnotesinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<utmsplanpricinggetnotesinfoOutput>> CreateAsync(utmsplanpricinggetnotesinfoInput req)
        {
            List<utmsplanpricinggetnotesinfoOutput> result = new List<utmsplanpricinggetnotesinfoOutput>();
            utmsplanpricinggetnotesinfoInput objSignInInput = new utmsplanpricinggetnotesinfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = utmsplanpricinggetnotesinfoDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                utmsplanpricinggetnotesinfoOutput outputobj = new utmsplanpricinggetnotesinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
