﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NLog; using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTMSDocker.DB;
using static UTMSDocker.Models.UtmscreatemenuinfoModel;
using static UTMSDocker.Models.UTMScreateRoleMenuMappingModel;

namespace UTMSDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UTMScreateRoleMenuMappingController : ControllerBase
    {
         
        public IConfiguration _Config { get; }  private readonly ILogger<UTMScreateRoleMenuMappingController> _logger;

        public UTMScreateRoleMenuMappingController(ILogger<UTMScreateRoleMenuMappingController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UTMScreateRoleMenuMappingOutput>> CreateAsync(UTMScreateRoleMenuMappingInput req)
        {
            List<UTMScreateRoleMenuMappingOutput> result = new List<UTMScreateRoleMenuMappingOutput>();
            UTMScreateRoleMenuMappingInput objSignInInput = new UTMScreateRoleMenuMappingInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UTMScreateRoleMenuMappingDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {
                UTMScreateRoleMenuMappingOutput outputobj = new UTMScreateRoleMenuMappingOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}
