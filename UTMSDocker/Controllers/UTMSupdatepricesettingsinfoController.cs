﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UTMSupdatepricesettingsinfoModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UTMSupdatepricesettingsinfoController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UTMSupdatepricesettingsinfoController> _logger;

        public UTMSupdatepricesettingsinfoController(ILogger<UTMSupdatepricesettingsinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UTMSupdatepricesettingsinfoOutput>> CreateAsync(UTMSupdatepricesettingsinfoInput req)
        {
            List<UTMSupdatepricesettingsinfoOutput> result = new List<UTMSupdatepricesettingsinfoOutput>();
            UTMSupdatepricesettingsinfoInput objSignInInput = new UTMSupdatepricesettingsinfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UTMSupdatepricesettingsinfoDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UTMSupdatepricesettingsinfoOutput outputobj = new UTMSupdatepricesettingsinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
