﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using UTMSDocker.DB;
using static UTMSDocker.Models.UtmscreateplanaddonfeatureinfoModel;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UtmscreateplanaddonfeatureinfoController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UtmscreateplanaddonfeatureinfoController> _logger;

        public UtmscreateplanaddonfeatureinfoController(ILogger<UtmscreateplanaddonfeatureinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UtmscreateplanaddonfeatureinfoOutput>> CreateAsync(UtmscreateplanaddonfeatureinfoInput req)
        {
            List<UtmscreateplanaddonfeatureinfoOutput> result = new List<UtmscreateplanaddonfeatureinfoOutput>();
            UtmscreateplanaddonfeatureinfoInput objSignInInput = new UtmscreateplanaddonfeatureinfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UtmscreateplanaddonfeatureinfoDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UtmscreateplanaddonfeatureinfoOutput outputobj = new UtmscreateplanaddonfeatureinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
