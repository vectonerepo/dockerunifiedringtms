﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NLog; using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTMSDocker.DB;
using static UTMSDocker.Models.UtmscreateesptariffModel;

namespace UTMSDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UtmscreateesptariffController : ControllerBase
    {
         
        public IConfiguration _Config { get; }  private readonly ILogger<UtmscreateesptariffController> _logger;

        public UtmscreateesptariffController(ILogger<UtmscreateesptariffController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UtmscreateesptariffOutput>> CreateAsync(UtmscreateesptariffInput req)
        {
            List<UtmscreateesptariffOutput> result = new List<UtmscreateesptariffOutput>();
            UtmscreateesptariffInput objSignInInput = new UtmscreateesptariffInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UtmscreateesptariffDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UtmscreateesptariffOutput outputobj = new UtmscreateesptariffOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}
