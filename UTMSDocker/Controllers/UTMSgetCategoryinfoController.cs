﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UTMSgetCategoryinfoModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UTMSgetCategoryinfoController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UTMSgetCategoryinfoController> _logger;

        public UTMSgetCategoryinfoController(ILogger<UTMSgetCategoryinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UTMSgetCategoryinfoOutput>> CreateAsync(UTMSgetCategoryinfoInput req)
        {
            List<UTMSgetCategoryinfoOutput> result = new List<UTMSgetCategoryinfoOutput>();
            UTMSgetCategoryinfoInput objSignInInput = new UTMSgetCategoryinfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UTMSgetCategoryinfoDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UTMSgetCategoryinfoOutput outputobj = new UTMSgetCategoryinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
