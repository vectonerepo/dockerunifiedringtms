﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using UTMSDocker.DB;
using static UTMSDocker.Models.UtmsuploadwebtariffratesInputModel;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UtmsuploadwebtariffratesController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UtmsuploadwebtariffratesController> _logger;

        public UtmsuploadwebtariffratesController(ILogger<UtmsuploadwebtariffratesController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UtmsuploadwebtariffratesOutput>> CreateAsync(UtmsuploadwebtariffratesInput req)
        {
            List<UtmsuploadwebtariffratesOutput> result = new List<UtmsuploadwebtariffratesOutput>();
            UtmsuploadwebtariffratesInput objSignInInput = new UtmsuploadwebtariffratesInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UtmsuploadwebtariffratesDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UtmsuploadwebtariffratesOutput outputobj = new UtmsuploadwebtariffratesOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
