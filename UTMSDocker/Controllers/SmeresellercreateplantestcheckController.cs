﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
//using UTMSDocker.Controller;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.SmeresellercreateplantestcheckModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class SmeresellercreateplantestcheckController : ControllerBase
    {
         
        public IConfiguration _Config { get; }  private readonly ILogger<SmeresellercreateplantestcheckController> _logger;

        public SmeresellercreateplantestcheckController(ILogger<SmeresellercreateplantestcheckController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<SmeresellercreateplantestcheckOutput>> CreateAsync(SmeresellercreateplantestcheckInput req)
        {
            _logger.LogInformation("Smeresellercreateplantestcheck:" + JsonConvert.SerializeObject(req));
            List<SmeresellercreateplantestcheckOutput> result = new List<SmeresellercreateplantestcheckOutput>();
            SmeresellercreateplantestcheckInput objSignInInput = new SmeresellercreateplantestcheckInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = SmeresellercreateplantestcheckDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {

                SmeresellercreateplantestcheckOutput outputobj = new SmeresellercreateplantestcheckOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
