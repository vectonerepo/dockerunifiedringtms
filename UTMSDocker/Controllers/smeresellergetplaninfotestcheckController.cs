﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
//using UTMSDocker.Controller;
using Microsoft.Extensions.Configuration;
using UTMSDocker.DB;
using static UTMSDocker.Models.SmeresellergetplaninfotestcheckModel;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class smeresellergetplaninfotestcheckController : ControllerBase
    {
         
        public IConfiguration _Config { get; }  private readonly ILogger<smeresellergetplaninfotestcheckController> _logger;

        public smeresellergetplaninfotestcheckController(ILogger<smeresellergetplaninfotestcheckController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<smeresellergetplaninfotestcheckOutput>> CreateAsync(SmeresellergetplaninfotestcheckInput req)
        {
            _logger.LogInformation("Input : Request controller:" + JsonConvert.SerializeObject(req));
            List<smeresellergetplaninfotestcheckOutput> result = new List<smeresellergetplaninfotestcheckOutput>();
            SmeresellergetplaninfotestcheckInput objSignInInput = new SmeresellergetplaninfotestcheckInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = SmeresellergetplaninfotestcheckDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {

                smeresellergetplaninfotestcheckOutput outputobj = new smeresellergetplaninfotestcheckOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
