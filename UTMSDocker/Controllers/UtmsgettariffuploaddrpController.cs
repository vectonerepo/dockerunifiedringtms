﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UtmsgettariffuploaddrpModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UtmsgettariffuploaddrpController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UtmsgettariffuploaddrpController> _logger;

        public UtmsgettariffuploaddrpController(ILogger<UtmsgettariffuploaddrpController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UtmsgettariffuploaddrpOutput>> CreateAsync(UtmsgettariffuploaddrpInput req)
        {
            List<UtmsgettariffuploaddrpOutput> result = new List<UtmsgettariffuploaddrpOutput>();
            UtmsgettariffuploaddrpInput objSignInInput = new UtmsgettariffuploaddrpInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UtmsgettariffuploaddrpDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UtmsgettariffuploaddrpOutput outputobj = new UtmsgettariffuploaddrpOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
