﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using NLog; using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTMSDocker.DB;
using static UTMSDocker.Models.UtmscreatecategoryinfoModel;

namespace UTMSDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UtmscreatecategoryinfoController : ControllerBase
    {
         
        public IConfiguration _Config { get; }  private readonly ILogger<UtmscreatecategoryinfoController> _logger;

        public UtmscreatecategoryinfoController(ILogger<UtmscreatecategoryinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UtmscreatecategoryinfoOutput>> CreateAsync(UtmscreatecategoryinfoInput req)
        {
            _logger.LogInformation("Utmscreatecategoryinfo:" + JsonConvert.SerializeObject(req));
            List<UtmscreatecategoryinfoOutput> result = new List<UtmscreatecategoryinfoOutput>();
            UtmscreatecategoryinfoInput objSignInInput = new UtmscreatecategoryinfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UtmscreatecategoryinfoDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UtmscreatecategoryinfoOutput outputobj = new UtmscreatecategoryinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}
