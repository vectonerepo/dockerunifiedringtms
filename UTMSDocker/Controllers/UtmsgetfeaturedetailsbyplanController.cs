﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UtmsgetfeaturedetailsbyplanModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UtmsgetfeaturedetailsbyplanController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UtmsgetfeaturedetailsbyplanController> _logger;

        public UtmsgetfeaturedetailsbyplanController(ILogger<UtmsgetfeaturedetailsbyplanController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UtmsgetfeaturedetailsbyplanOutput>> CreateAsync(UtmsgetfeaturedetailsbyplanInput req)
        {
            List<UtmsgetfeaturedetailsbyplanOutput> result = new List<UtmsgetfeaturedetailsbyplanOutput>();
            UtmsgetfeaturedetailsbyplanInput objSignInInput = new UtmsgetfeaturedetailsbyplanInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UtmsgetfeaturedetailsbyplanDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UtmsgetfeaturedetailsbyplanOutput outputobj = new UtmsgetfeaturedetailsbyplanOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
