﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog;
using Newtonsoft.Json;
//using UTMSDocker.Controller;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UrtmsgetresellerdiscountinfoController : ControllerBase
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        //  private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrtmsgetresellerdiscountinfoInput
        {
            public int product_id { get; set; }
        }
        public class UrtmsgetresellerdiscountinfoOutput : ErrorMessageModel
        {
            public int? user_range_from { get; set; }
            public int? user_range_to { get; set; }
            public double? discount { get; set; }
        }
        public class ErrorMessageModel
        {
            public string Message { get; set; }
            public int? Code { get; set; }
        }
        public IConfiguration _Config { get; }
        private readonly ILogger<UrtmsgetresellerdiscountinfoController> _logger;

        public UrtmsgetresellerdiscountinfoController(ILogger<UrtmsgetresellerdiscountinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;
            _logger = logger;
        }
  
      


        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UrtmsgetresellerdiscountinfoOutput>> CreateAsync(UrtmsgetresellerdiscountinfoInput req)
        {
            //Log.Info("Input : Request controller:" + JsonConvert.SerializeObject(req));
            _logger.LogInformation("Input : UrtmsgetresellerdiscountinfoController :" + JsonConvert.SerializeObject(req));

            List<UrtmsgetresellerdiscountinfoOutput> result = new List<UrtmsgetresellerdiscountinfoOutput>();
            UrtmsgetresellerdiscountinfoInput objSignInInput = new UrtmsgetresellerdiscountinfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrtmsgetresellerdiscountinfoDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {

                UrtmsgetresellerdiscountinfoOutput outputobj = new UrtmsgetresellerdiscountinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
