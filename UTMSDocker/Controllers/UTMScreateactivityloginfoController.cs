﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using NLog; using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTMSDocker.DB;
using static UTMSDocker.Models.UTMScreateactivityloginfoModel;

namespace UTMSDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UTMScreateactivityloginfoController : ControllerBase
    {
         
        public IConfiguration _Config { get; }  private readonly ILogger<UTMScreateactivityloginfoController> _logger;

        public UTMScreateactivityloginfoController(ILogger<UTMScreateactivityloginfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UTMScreateactivityloginfoOutput>> CreateAsync(UTMScreateactivityloginfoInput req)
        {
            _logger.LogInformation("UTMScreateactivityloginfo:" + JsonConvert.SerializeObject(req));
            List<UTMScreateactivityloginfoOutput> result = new List<UTMScreateactivityloginfoOutput>();
            UTMScreateactivityloginfoInput objSignInInput = new UTMScreateactivityloginfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UTMScreateactivityloginfoDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UTMScreateactivityloginfoOutput outputobj = new UTMScreateactivityloginfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}
