﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UTMSupdateDeleteWEBratedetailsModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UTMSupdateDeleteWEBratedetailsController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UTMSupdateDeleteWEBratedetailsController> _logger;

        public UTMSupdateDeleteWEBratedetailsController(ILogger<UTMSupdateDeleteWEBratedetailsController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UTMSupdateDeleteWEBratedetailsOutput>> CreateAsync(UTMSupdateDeleteWEBratedetailsInput req)
        {
            List<UTMSupdateDeleteWEBratedetailsOutput> result = new List<UTMSupdateDeleteWEBratedetailsOutput>();
            UTMSupdateDeleteWEBratedetailsInput objSignInInput = new UTMSupdateDeleteWEBratedetailsInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UTMSupdateDeleteWEBratedetailsDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UTMSupdateDeleteWEBratedetailsOutput outputobj = new UTMSupdateDeleteWEBratedetailsOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
