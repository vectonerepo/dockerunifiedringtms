﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UtmsrolemenumappingModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UtmsrolemenumappingController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UtmsrolemenumappingController> _logger;

        public UtmsrolemenumappingController(ILogger<UtmsrolemenumappingController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UtmsrolemenumappingOutput>> CreateAsync(UtmsrolemenumappingInput req)
        {
            List<UtmsrolemenumappingOutput> result = new List<UtmsrolemenumappingOutput>();
            UtmsrolemenumappingInput objSignInInput = new UtmsrolemenumappingInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UtmsrolemenumappingDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UtmsrolemenumappingOutput outputobj = new UtmsrolemenumappingOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
