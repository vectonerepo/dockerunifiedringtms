﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using UTMSDocker.DB;
using static UTMSDocker.Models.UTMScreateplaninfoModel;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UTMScreateplaninfoController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UTMScreateplaninfoController> _logger;

        public UTMScreateplaninfoController(ILogger<UTMScreateplaninfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UTMScreateplaninfoOutput>> CreateAsync(UTMScreateplaninfoInput req)
        {
            List<UTMScreateplaninfoOutput> result = new List<UTMScreateplaninfoOutput>();
            UTMScreateplaninfoInput objSignInInput = new UTMScreateplaninfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UTMScreateplaninfoDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UTMScreateplaninfoOutput outputobj = new UTMScreateplaninfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
