﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UtmsinsertuserrangemasterdetailModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UtmsinsertuserrangemasterdetailController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UtmsinsertuserrangemasterdetailController> _logger;

        public UtmsinsertuserrangemasterdetailController(ILogger<UtmsinsertuserrangemasterdetailController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UtmsinsertuserrangemasterdetailOutput>> CreateAsync(UtmsinsertuserrangemasterdetailInput req)
        {
            List<UtmsinsertuserrangemasterdetailOutput> result = new List<UtmsinsertuserrangemasterdetailOutput>();
            UtmsinsertuserrangemasterdetailInput objSignInInput = new UtmsinsertuserrangemasterdetailInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UtmsinsertuserrangemasterdetailDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UtmsinsertuserrangemasterdetailOutput outputobj = new UtmsinsertuserrangemasterdetailOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
