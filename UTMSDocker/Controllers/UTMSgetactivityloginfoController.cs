﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UTMSgetactivityloginfoModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UTMSgetactivityloginfoController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UTMSgetactivityloginfoController> _logger;

        public UTMSgetactivityloginfoController(ILogger<UTMSgetactivityloginfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UTMSgetactivityloginfoOutput>> CreateAsync(UTMSgetactivityloginfoInput req)
        {
            List<UTMSgetactivityloginfoOutput> result = new List<UTMSgetactivityloginfoOutput>();
            UTMSgetactivityloginfoInput objSignInInput = new UTMSgetactivityloginfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UTMSgetactivityloginfoDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UTMSgetactivityloginfoOutput outputobj = new UTMSgetactivityloginfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
