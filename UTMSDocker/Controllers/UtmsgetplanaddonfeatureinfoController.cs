﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UtmsgetplanaddonfeatureinfoModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UtmsgetplanaddonfeatureinfoController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UtmsgetplanaddonfeatureinfoController> _logger;

        public UtmsgetplanaddonfeatureinfoController(ILogger<UtmsgetplanaddonfeatureinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UtmsgetplanaddonfeatureinfoOutput>> CreateAsync(UtmsgetplanaddonfeatureinfoInput req)
        {
            List<UtmsgetplanaddonfeatureinfoOutput> result = new List<UtmsgetplanaddonfeatureinfoOutput>();
            UtmsgetplanaddonfeatureinfoInput objSignInInput = new UtmsgetplanaddonfeatureinfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UtmsgetplanaddonfeatureinfoDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UtmsgetplanaddonfeatureinfoOutput outputobj = new UtmsgetplanaddonfeatureinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
