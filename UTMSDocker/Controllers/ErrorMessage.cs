﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTMSDocker.Controllers
{
    public static class ErrorMessage
    {
        public static Dictionary<int, string> Errors = new Dictionary<int, string>()
                                                {
                                                    {1001,"Success"},
                                                    {1002, "Failure"},
                                                    {1003,"No record found"},
                                                    {1004,"Redis server error"},
                                                    {1005,"Exception,"},
                                                    {1006,"Google Email Verification Failed"},
                                                    {1007,"DB Error"},
                                                    {1008,""},
                                                    {1009,"Email ID not in the correct format"}
                                                };
    }
   

}