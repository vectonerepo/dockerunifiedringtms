﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UtmsgettariffratedownloadModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UtmsgettariffratedownloadController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UtmsgettariffratedownloadController> _logger;

        public UtmsgettariffratedownloadController(ILogger<UtmsgettariffratedownloadController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UtmsgettariffratedownloadOutput>> CreateAsync(UtmsgettariffratedownloadInput req)
        {
            List<UtmsgettariffratedownloadOutput> result = new List<UtmsgettariffratedownloadOutput>();
            UtmsgettariffratedownloadInput objSignInInput = new UtmsgettariffratedownloadInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UtmsgettariffratedownloadDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UtmsgettariffratedownloadOutput outputobj = new UtmsgettariffratedownloadOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
