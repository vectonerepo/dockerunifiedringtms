﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using NLog; using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTMSDocker.DB;
using static UTMSDocker.Models.UTMScreatefeaturedetailModel;

namespace UTMSDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UTMScreatefeaturedetailController : ControllerBase
    {

         
        public IConfiguration _Config { get; }  private readonly ILogger<UTMScreatefeaturedetailController> _logger;

        public UTMScreatefeaturedetailController(ILogger<UTMScreatefeaturedetailController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UTMScreatefeaturedetailOutput>> CreateAsync(UTMScreatefeaturedetailInput req)
        {
            List<UTMScreatefeaturedetailOutput> result = new List<UTMScreatefeaturedetailOutput>();
            UTMScreatefeaturedetailInput objSignInInput = new UTMScreatefeaturedetailInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UTMScreatefeaturedetailDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UTMScreatefeaturedetailOutput outputobj = new UTMScreatefeaturedetailOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}
