﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using UTMSDocker.DB;
using static UTMSDocker.Models.UTMSGetMenuInfoModel;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UTMSGetMenuInfoController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UTMSGetMenuInfoController> _logger;

        public UTMSGetMenuInfoController(ILogger<UTMSGetMenuInfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UTMSGetMenuInfoOutput>> CreateAsync(UTMSGetMenuInfoInput req)
        {
            List<UTMSGetMenuInfoOutput> result = new List<UTMSGetMenuInfoOutput>();
            UTMSGetMenuInfoInput objSignInInput = new UTMSGetMenuInfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UTMSGetMenuInfoDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UTMSGetMenuInfoOutput outputobj = new UTMSGetMenuInfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
