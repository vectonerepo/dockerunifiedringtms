﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UtmsportalgetbundlelistinfoModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UtmsportalgetbundlelistinfoController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UtmsportalgetbundlelistinfoController> _logger;

        public UtmsportalgetbundlelistinfoController(ILogger<UtmsportalgetbundlelistinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UtmsportalgetbundlelistinfoOutput>> CreateAsync(UtmsportalgetbundlelistinfoInput req)
        {
            List<UtmsportalgetbundlelistinfoOutput> result = new List<UtmsportalgetbundlelistinfoOutput>();
            UtmsportalgetbundlelistinfoInput objSignInInput = new UtmsportalgetbundlelistinfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UtmsportalgetbundlelistinfoDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UtmsportalgetbundlelistinfoOutput outputobj = new UtmsportalgetbundlelistinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
