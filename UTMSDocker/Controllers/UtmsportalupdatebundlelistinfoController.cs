﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UtmsportalupdatebundlelistinfoModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UtmsportalupdatebundlelistinfoController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UtmsportalupdatebundlelistinfoController> _logger;  

        public UtmsportalupdatebundlelistinfoController(ILogger<UtmsportalupdatebundlelistinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UtmsportalupdatebundlelistinfoOutput>> CreateAsync(UtmsportalupdatebundlelistinfoInput req)
        {
            List<UtmsportalupdatebundlelistinfoOutput> result = new List<UtmsportalupdatebundlelistinfoOutput>();
            UtmsportalupdatebundlelistinfoInput objSignInInput = new UtmsportalupdatebundlelistinfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UtmsportalupdatebundlelistinfoDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UtmsportalupdatebundlelistinfoOutput outputobj = new UtmsportalupdatebundlelistinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
