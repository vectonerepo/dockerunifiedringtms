﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UTMSgetplanpricebyuserRangeModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UTMSgetplanpricebyuserRangeController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UTMSgetplanpricebyuserRangeController> _logger;

        public UTMSgetplanpricebyuserRangeController(ILogger<UTMSgetplanpricebyuserRangeController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UTMSgetplanpricebyuserRangeOutput>> CreateAsync(UTMSgetplanpricebyuserRangeInput req)
        {
            List<UTMSgetplanpricebyuserRangeOutput> result = new List<UTMSgetplanpricebyuserRangeOutput>();
            UTMSgetplanpricebyuserRangeInput objSignInInput = new UTMSgetplanpricebyuserRangeInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UTMSgetplanpricebyuserRangeDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UTMSgetplanpricebyuserRangeOutput outputobj = new UTMSgetplanpricebyuserRangeOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
