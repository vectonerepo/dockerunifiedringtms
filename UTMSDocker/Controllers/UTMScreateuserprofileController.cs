﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UTMScreateuserprofileModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UTMScreateuserprofileController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UTMScreateuserprofileController> _logger;

        public UTMScreateuserprofileController(ILogger<UTMScreateuserprofileController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UTMScreateuserprofileOutput>> CreateAsync(UTMScreateuserprofileInput req)
        {
            List<UTMScreateuserprofileOutput> result = new List<UTMScreateuserprofileOutput>();
            UTMScreateuserprofileInput objSignInInput = new UTMScreateuserprofileInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UTMScreateuserprofileDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UTMScreateuserprofileOutput outputobj = new UTMScreateuserprofileOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
