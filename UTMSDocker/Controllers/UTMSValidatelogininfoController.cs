﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UTMSValidatelogininfoModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]  
    [Authorize]
    public class UTMSValidatelogininfoController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UTMSValidatelogininfoController> _logger;

        public UTMSValidatelogininfoController(ILogger<UTMSValidatelogininfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        //   public async Task<ActionResult<UTMSValidatelogininfoOutput>> CreateAsync(UTMSValidatelogininfoInput req) 
        public async Task<ActionResult<UTMSValidatelogininfoOutput>> Post(UTMSValidatelogininfoInput req)
        {
            List<UTMSValidatelogininfoOutput> result = new List<UTMSValidatelogininfoOutput>();
            UTMSValidatelogininfoInput objSignInInput = new UTMSValidatelogininfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UTMSValidatelogininfoDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UTMSValidatelogininfoOutput outputobj = new UTMSValidatelogininfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
