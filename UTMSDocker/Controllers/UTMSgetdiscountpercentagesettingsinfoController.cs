﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NLog; using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTMSDocker.DB;
using static UTMSDocker.Models.UTMSgetdiscountpercentagesettingsinfoModel;

namespace UTMSDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UTMSgetdiscountpercentagesettingsinfoController : ControllerBase
    {
         
        public IConfiguration _Config { get; }  private readonly ILogger<UTMSgetdiscountpercentagesettingsinfoController> _logger;

        public UTMSgetdiscountpercentagesettingsinfoController(ILogger<UTMSgetdiscountpercentagesettingsinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<UTMSgetdiscountpercentagesettingsinfoOutput>> CreateAsync()
        {
            List<UTMSgetdiscountpercentagesettingsinfoOutput> result = new List<UTMSgetdiscountpercentagesettingsinfoOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UTMSgetdiscountpercentagesettingsinfoDB.CallDB(_Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UTMSgetdiscountpercentagesettingsinfoOutput outputobj = new UTMSgetdiscountpercentagesettingsinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}
