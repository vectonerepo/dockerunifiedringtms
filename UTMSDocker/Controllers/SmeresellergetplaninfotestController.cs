﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.SmeresellergetplaninfotestcheckModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class SmeresellergetplaninfotestController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<SmeresellergetplaninfotestController> _logger;  

        public SmeresellergetplaninfotestController(ILogger<SmeresellergetplaninfotestController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<smeresellergetplaninfotestcheckOutput>> CreateAsync(SmeresellergetplaninfotestcheckInput req)
        {
            List<smeresellergetplaninfotestcheckOutput> result = new List<smeresellergetplaninfotestcheckOutput>();
            SmeresellergetplaninfotestcheckInput objSignInInput = new SmeresellergetplaninfotestcheckInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = SmeresellergetplaninfotestcheckDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                smeresellergetplaninfotestcheckOutput outputobj = new smeresellergetplaninfotestcheckOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
