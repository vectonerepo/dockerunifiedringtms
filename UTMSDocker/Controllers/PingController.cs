﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UTMSDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PingController : ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<PingController> _logger;

        public PingController(ILogger<PingController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            //var rng = new Random();
            //return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            //{
            //    Date = DateTime.Now.AddDays(index),
            //    TemperatureC = rng.Next(-20, 55),
            //    Summary = Summaries[rng.Next(Summaries.Length)]
            //})
            //.ToArray();
          //  return new string[] { "Docker initiated in " + _Config["ConnectionStrings:UTMSAPP"] };

            string connectionType = _Config["Connectiontype:value"];
            string connection = _Config["ConnectionStrings:UTMSAPP"];

            return new string[] { "Docker initiated in  " + connectionType + "-" + connection };
        }
    }
}
