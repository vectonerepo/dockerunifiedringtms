﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UtmsplanpricingcreatenotesinfoModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UtmsplanpricingcreatenotesinfoController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UtmsplanpricingcreatenotesinfoController> _logger;

        public UtmsplanpricingcreatenotesinfoController(ILogger<UtmsplanpricingcreatenotesinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UtmsplanpricingcreatenotesinfoOutput>> CreateAsync(UtmsplanpricingcreatenotesinfoInput req)
        {
            List<UtmsplanpricingcreatenotesinfoOutput> result = new List<UtmsplanpricingcreatenotesinfoOutput>();
            UtmsplanpricingcreatenotesinfoInput objSignInInput = new UtmsplanpricingcreatenotesinfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UtmsplanpricingcreatenotesinfoDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UtmsplanpricingcreatenotesinfoOutput outputobj = new UtmsplanpricingcreatenotesinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
