﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UTMSValidateuserroleinfoModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UTMSValidateuserroleinfoController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UTMSValidateuserroleinfoController> _logger;

        public UTMSValidateuserroleinfoController(ILogger<UTMSValidateuserroleinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UTMSValidateuserroleinfoOutput>> CreateAsync(UTMSValidateuserroleinfoInput req)
        {
            List<UTMSValidateuserroleinfoOutput> result = new List<UTMSValidateuserroleinfoOutput>();
            UTMSValidateuserroleinfoInput objSignInInput = new UTMSValidateuserroleinfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UTMSValidateuserroleinfoDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UTMSValidateuserroleinfoOutput outputobj = new UTMSValidateuserroleinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
