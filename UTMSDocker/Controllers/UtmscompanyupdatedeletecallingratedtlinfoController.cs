﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
//using UTMSDocker.Controller;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.SmeresellercreateplantestcheckModel;
using UTMSDocker.DB;
using static UTMSDocker.Models.UtmscompanyupdatedeletecallingratedtlinfoModel;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UtmscompanyupdatedeletecallingratedtlinfoController : ControllerBase
    {
         
        public IConfiguration _Config { get; }  private readonly ILogger<UtmscompanyupdatedeletecallingratedtlinfoController> _logger;

        public UtmscompanyupdatedeletecallingratedtlinfoController(ILogger<UtmscompanyupdatedeletecallingratedtlinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UtmscompanyupdatedeletecallingratedtlinfoOutput>> CreateAsync(UtmscompanyupdatedeletecallingratedtlinfoInput req)
        {
            _logger.LogInformation("Utmscompanyupdatedeletecallingratedtlinfo:" + JsonConvert.SerializeObject(req));
            List<UtmscompanyupdatedeletecallingratedtlinfoOutput> result = new List<UtmscompanyupdatedeletecallingratedtlinfoOutput>();
            UtmscompanyupdatedeletecallingratedtlinfoInput objSignInInput = new UtmscompanyupdatedeletecallingratedtlinfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UtmscompanyupdatedeletecallingratedtlinfoDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {

                UtmscompanyupdatedeletecallingratedtlinfoOutput outputobj = new UtmscompanyupdatedeletecallingratedtlinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
