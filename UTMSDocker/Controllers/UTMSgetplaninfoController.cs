﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UTMSgetplaninfoModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UTMSgetplaninfoController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UTMSgetplaninfoController> _logger;

        public UTMSgetplaninfoController(ILogger<UTMSgetplaninfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UTMSgetplaninfoOutput>> CreateAsync(UTMSgetplaninfoInput req)
        {
            List<UTMSgetplaninfoOutput> result = new List<UTMSgetplaninfoOutput>();
            UTMSgetplaninfoInput objSignInInput = new UTMSgetplaninfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UTMSgetplaninfoDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UTMSgetplaninfoOutput outputobj = new UTMSgetplaninfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
