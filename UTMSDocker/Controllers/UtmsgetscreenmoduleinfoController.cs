﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NLog; using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using static UTMSDocker.Models.UtmsgetscreenmoduleinfoModel;
using UTMSDocker.DB;

namespace UTMSDocker
{
    [Route("api/[controller]")]
    [ApiController]    
    public class UtmsgetscreenmoduleinfoController : ControllerBase
    {
         
        
        public IConfiguration _Config { get; }  private readonly ILogger<UtmsgetscreenmoduleinfoController> _logger;

        public UtmsgetscreenmoduleinfoController(ILogger<UtmsgetscreenmoduleinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration;   _logger = logger;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UtmsgetscreenmoduleinfoOutput>> CreateAsync(UtmsgetscreenmoduleinfoInput req)
        {
            List<UtmsgetscreenmoduleinfoOutput> result = new List<UtmsgetscreenmoduleinfoOutput>();
            UtmsgetscreenmoduleinfoInput objSignInInput = new UtmsgetscreenmoduleinfoInput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UtmsgetscreenmoduleinfoDB.CallDB(req, _Config,_logger);
                return result.Count > 0 ? Ok(result) : NoContent(); 
            }
            catch (Exception ex)
            {
                UtmsgetscreenmoduleinfoOutput outputobj = new UtmsgetscreenmoduleinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                //_logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }


}
