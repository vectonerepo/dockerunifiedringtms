﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;
using System.Data;
using Microsoft.Extensions.Configuration;

namespace DBConnection
{
    public static class CheckDB
    {
        public static void CallDBConnection(IConfiguration _Config,string sp, object param, ref IEnumerable<dynamic> result,string connectionstring)
        {
            string connectionType = _Config["Connectiontype:value"];
            
            string connection = _Config[connectionstring];

            if (connectionType.ToLower() == "mysql")
            {
                using (var mysqlconnection = new MySqlConnection(connection))
                {
                    mysqlconnection.Open();
                    result = mysqlconnection.Query<dynamic>(
                           sp, param,
                           commandType: CommandType.StoredProcedure);
                }
            }
            else
            {
                
                using (var sqlconnection = new SqlConnection(connection))
                {
                    sqlconnection.Open();
                    result = sqlconnection.Query<dynamic>(
                           sp, param,
                           commandType: CommandType.StoredProcedure);
                }
            }
        }
    }
}