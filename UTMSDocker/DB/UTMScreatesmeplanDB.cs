﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using static UTMSDocker.Models.UTMScreatesmeplanModel;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    //UTMScreatesmeplan
    public class UTMScreatesmeplanDB
    {

        public static List<UTMScreatesmeplanOutput> CallDB(UTMScreatesmeplanInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UTMScreatesmeplanOutput> OutputList = new List<UTMScreatesmeplanOutput>();
            _logger.LogInformation("Input : UTMScreatesmeplanController:" + JsonConvert.SerializeObject(req));
            try
            {

                var sp = "UTMS_create_sme_plan";
                object param = new

                {
                    @yearly_discount = req.yearly_discount,
                    @monthly_yearly_discount = req.monthly_yearly_discount,
                    @created_by = req.created_by,
                    @status = req.status,
                    @Audio_conf = req.Audio_conf,
                    @Video_conf = req.Video_conf,
                    @Audio_conf_value = req.Audio_conf_value,
                    @Video_conf_value = req.Video_conf_value,
                    @Calling_UK_mins = req.Calling_UK_mins,
                    @User_Id = req.User_Id,
                    @Product_Id = req.Product_Id,
                    @Pc_From_Users = req.Pc_From_Users,
                    @Pc_To_Users = req.Pc_To_Users,
                    @PC_Price = req.PC_Price,
                    @features = req.features,
                    @Process_Type = req.Process_Type,
                    @National_min = req.National_min,
                    @Geo_Min = req.Geo_Min,
                    @Plan_Name = req.Plan_Name,
                    @Request_ID = req.Request_ID,
                    @Unlimted_Intra_Call = req.Unlimted_Intra_Call,
                    @is_free_trial = req.is_free_trial,
                    @monthly_price = req.monthly_price,
                    @yearly_price = req.yearly_price,
                    @monthly_yearly_price = req.monthly_yearly_price,
                    @most_popular = req.most_popular,
                    @freeph_non_geo_loc_min = req.freeph_non_geo_loc_min,
                    @Unlimited_inbound_conf = req.Unlimited_inbound_conf,
                    @Unlimited_inbound_conf_value = req.Unlimited_inbound_conf_value,
                    @free_trial_days = req.free_trial_days,
                    @is_reseller = req.is_reseller,
                    @FT_audio_conf = req.FT_audio_conf,
                    @FT_audio_conf_value = req.FT_audio_conf_value,
                    @FT_video_conf = req.FT_video_conf,
                    @FT_video_conf_value = req.FT_video_conf_value,
                    @FT_Unlimted_Intra_Call = req.FT_Unlimted_Intra_Call,
                    @FT_Unlimted_Intra_Call_min = req.FT_Unlimted_Intra_Call_min,
                    @Free_trial_min = req.Free_trial_min,
                    @reseller_discount_flag = req.reseller_discount_flag,
                    @reseller_discount_xml = req.reseller_discount_xml,
                    @two_year_monthly_price = req.two_year_monthly_price,
                    @three_year_monthly_price = req.three_year_monthly_price,
                    @two_year_monthly_discount = req.two_year_monthly_discount,
                    @three_year_monthly_discount = req.three_year_monthly_discount,
                    @User_Range_xml = req.User_Range_xml,
                    @plan_addon_price_xml = req.plan_addon_price_xml,
                    @Video_conf_meetings = req.Video_conf_meetings,
                    @Video_conf_participants = req.Video_conf_participants,
                    @Audio_conf_participants = req.Audio_conf_participants,
                    @FT_video_conf_meetings = req.FT_video_conf_meetings,
                    @FT_video_conf_participants = req.FT_video_conf_participants,
                    @FT_Audio_conf_participants = req.FT_Audio_conf_participants,
                    @Feature_xml = req.Feature_xml,
                    @Video_conf_duration = req.Video_conf_duration


                };
                IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
                {
                    _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                    OutputList.AddRange(result.Select(r => new UTMScreatesmeplanOutput()
                    {
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg
                    }));
                }
                else
                {
                    _logger.LogInformation("Output DB : " + "Empty result");
                    UTMScreatesmeplanOutput outputobj = new UTMScreatesmeplanOutput();
                    outputobj.Code = -1;
                    outputobj.Message = "No Rec found";
                    OutputList.Add(outputobj);
                }
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UTMScreatesmeplanOutput outputobj = new UTMScreatesmeplanOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }

    }
}