﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using static UTMSDocker.Models.UtmsgetplanaddonbyuserModel;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UtmsgetplanaddonbyuserDB
    {

        public static List<UtmsgetplanaddonbyuserOutput> CallDB(UtmsgetplanaddonbyuserInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UtmsgetplanaddonbyuserOutput> OutputList = new List<UtmsgetplanaddonbyuserOutput>();
            _logger.LogInformation("Input : UtmsgetplanaddonbyuserController:" + JsonConvert.SerializeObject(req));
            try
            {

                var sp = "Utms_get_plan_addon_by_user";
                object param = new

                {
                    @pc_planid = req.pc_planid
                };
                IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
                {
                    _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                    OutputList.AddRange(result.Select(r => new UtmsgetplanaddonbyuserOutput()
                    {
                        pc_id = r.pc_id == null ? 0 : r.pc_id,
                        pc_planid = r.pc_planid == null ? 0 : r.pc_planid,
                        pc_addon_id = r.pc_addon_id == null ? 0 : r.pc_addon_id,
                        pc_addon_feature_name = r.pc_addon_feature_name,
                        pc_addon_flag = r.pc_addon_flag == null ? 0 : r.pc_addon_flag,
                        pc_addon_price = r.pc_addon_price == null ? 0.0D : r.pc_addon_price,
                        create_date = r.create_date == null ? null : r.create_date,
                        create_by = r.create_by,
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg
                    }));
                }
                else
                {
                    _logger.LogInformation("Output DB : " + "Empty result");
                    UtmsgetplanaddonbyuserOutput outputobj = new UtmsgetplanaddonbyuserOutput();
                    outputobj.Code = -1;
                    outputobj.Message = "No Rec found";
                    OutputList.Add(outputobj);
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UtmsgetplanaddonbyuserOutput outputobj = new UtmsgetplanaddonbyuserOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}