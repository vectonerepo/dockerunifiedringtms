﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using static UTMSDocker.Models.UtmscreateplanaddonfeatureinfoModel;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UtmscreateplanaddonfeatureinfoDB
    {


        public static List<UtmscreateplanaddonfeatureinfoOutput> CallDB(UtmscreateplanaddonfeatureinfoInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UtmscreateplanaddonfeatureinfoOutput> OutputList = new List<UtmscreateplanaddonfeatureinfoOutput>();
            _logger.LogInformation("Input : UtmscreateplanaddonfeatureinfoController:" + JsonConvert.SerializeObject(req));
            try
            {

                var sp = "UTMS_create_Plan_addon_feature_info";
                object param = new

                {
                    @addon_avail_at = req.addon_avail_at,
                    @addon_feature_Name = req.addon_feature_Name,
                    @addon_feature_desc = req.addon_feature_desc,
                    @addon_status = req.addon_status,
                    @process_type = req.process_type,
                    @addon_id = req.addon_id,
                    @addon_price = req.addon_price,
                    @is_third_party = req.is_third_party,
                    @Company_addon_price = req.Company_addon_price,

                    @groupname = req.groupname,
                    @no_group_allowed = req.no_group_allowed,
                    @group_price = req.group_price,
                    @max_user_in_group = req.max_user_in_group,
                    @members_type = req.members_type,
                    @group_within_plan = req.group_within_plan,

                    @Feature_Type = req.Feature_Type,
                    @Feature_limit = req.Feature_limit,
                    @Tariff = req.Tariff,
                    @category_id = req.category_id,
                    @integrate_name = req.integrate_name,
                    @limit_set_text = req.limit_set_text,
                    @limit_set_value = req.limit_set_value
                };
                IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
                {
                    _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                    OutputList.AddRange(result.Select(r => new UtmscreateplanaddonfeatureinfoOutput()
                    {
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg
                    }));
                }
                else
                {
                    _logger.LogInformation("Output DB : " + "Empty result");
                    UtmscreateplanaddonfeatureinfoOutput outputobj = new UtmscreateplanaddonfeatureinfoOutput();
                    outputobj.Code = -1;
                    outputobj.Message = "No Rec found";
                    OutputList.Add(outputobj);
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UtmscreateplanaddonfeatureinfoOutput outputobj = new UtmscreateplanaddonfeatureinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}