﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using static UTMSDocker.Models.UtmsgettariffuploaddrpModel;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UtmsgettariffuploaddrpDB
    {
        public static List<UtmsgettariffuploaddrpOutput> CallDB(UtmsgettariffuploaddrpInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UtmsgettariffuploaddrpOutput> OutputList = new List<UtmsgettariffuploaddrpOutput>();
            _logger.LogInformation("Input : UtmsgettariffuploaddrpController:" + JsonConvert.SerializeObject(req));
            try
            {

                var sp = "utms_get_tariff_upload_drp";
                object param = new
                {
                    @type = req.type
                };
                IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
                {
                    _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                    OutputList.AddRange(result.Select(r => new UtmsgettariffuploaddrpOutput()
                    {
                        id = r.id == null ? 0 : r.id,
                        value = r.value,
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg
                    }));
                }
                else
                {
                    _logger.LogInformation("Output DB : " + "Empty result");
                    UtmsgettariffuploaddrpOutput outputobj = new UtmsgettariffuploaddrpOutput();
                    outputobj.Code = -1;
                    outputobj.Message = "No Rec found";
                    OutputList.Add(outputobj);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UtmsgettariffuploaddrpOutput outputobj = new UtmsgettariffuploaddrpOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }

    }
}