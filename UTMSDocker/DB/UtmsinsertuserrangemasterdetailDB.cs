﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using static UTMSDocker.Models.UtmsinsertuserrangemasterdetailModel;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UtmsinsertuserrangemasterdetailDB
    {

         

        public static List<UtmsinsertuserrangemasterdetailOutput> CallDB(UtmsinsertuserrangemasterdetailInput req,IConfiguration _Config,ILogger _logger)
        {
            List<UtmsinsertuserrangemasterdetailOutput> OutputList = new List<UtmsinsertuserrangemasterdetailOutput>();
            _logger.LogInformation("Input : UtmsinsertuserrangemasterdetailController:" + JsonConvert.SerializeObject(req));
            try
            {
              
                    var sp = "UTMS_insert_User_Range_Master_Detail";
                    object param = new 
                    {
                        @Range_typeid = req.Range_typeid,
                        @From_user_Range = req.From_user_Range,
                        @To_user_Range = req.To_user_Range,
                        @processtype = req.processtype
                    };
                    IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP");  if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UtmsinsertuserrangemasterdetailOutput()
                        {
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        UtmsinsertuserrangemasterdetailOutput outputobj = new UtmsinsertuserrangemasterdetailOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UtmsinsertuserrangemasterdetailOutput outputobj = new UtmsinsertuserrangemasterdetailOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}