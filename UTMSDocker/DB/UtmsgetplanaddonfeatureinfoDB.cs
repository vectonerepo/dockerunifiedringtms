﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using static UTMSDocker.Models.UtmsgetplanaddonfeatureinfoModel;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UtmsgetplanaddonfeatureinfoDB
    {


        public static List<UtmsgetplanaddonfeatureinfoOutput> CallDB(UtmsgetplanaddonfeatureinfoInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UtmsgetplanaddonfeatureinfoOutput> OutputList = new List<UtmsgetplanaddonfeatureinfoOutput>();
            _logger.LogInformation("Input : UtmsgetplanaddonfeatureinfoController:" + JsonConvert.SerializeObject(req));
            try
            {

                var sp = "UTMS_get_Plan_addon_feature_info";
                object param = new

                {
                    @pc_addon_id = req.pc_addon_id,
                    @pc_addon_available_level = req.pc_addon_available_level,
                    @category_id = req.category_id
                };
                IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
                {
                    _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                    OutputList.AddRange(result.Select(r => new UtmsgetplanaddonfeatureinfoOutput()
                    {
                        pc_addon_id = r.pc_addon_id == null ? 0 : r.pc_addon_id,
                        pc_addon_available_level = r.pc_addon_available_level == null ? 0 : r.pc_addon_available_level,
                        pc_addon_feature_name = r.pc_addon_feature_name,
                        pc_addon_feature_desc = r.pc_addon_feature_desc,
                        pc_addon_status = r.pc_addon_status == null ? 0 : r.pc_addon_status,
                        pc_addon_price = r.pc_addon_price == null ? 0.0D : r.pc_addon_price,
                        is_third_party = r.is_third_party == null ? 0 : r.is_third_party,
                        Company_addon_price = r.Company_addon_price == null ? 0.0D : r.Company_addon_price,
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg,

                        group_name = r.group_name,
                        max_user_group = r.max_user_group == null ? 0 : r.max_user_group,
                        no_group_allowed = r.no_group_allowed == null ? 0 : r.no_group_allowed,
                        group_price = r.group_price == null ? 0.0D : r.group_price,
                        group_within_plan = r.group_within_plan == null ? 0.0D : r.group_within_plan,
                        grp_member_type = r.grp_member_type,
                        Feature_Type = r.Feature_Type == null ? 0.0D : r.Feature_Type,
                        Feature_limit = r.Feature_limit == null ? 0.0D : r.Feature_limit,
                        tariff = r.tariff,
                        category_id = r.category_id == null ? 0 : r.category_id,
                        Integration_name = r.Integration_name,
                        limit_set_text = r.limit_set_text,
                        limit_set_value = r.limit_set_value == null ? 0.0D : r.limit_set_value,
                        category_name = r.category_name

                    }));



                }
                else
                {
                    _logger.LogInformation("Output DB : " + "Empty result");
                    UtmsgetplanaddonfeatureinfoOutput outputobj = new UtmsgetplanaddonfeatureinfoOutput();
                    outputobj.Code = -1;
                    outputobj.Message = "No Rec found";
                    OutputList.Add(outputobj);
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UtmsgetplanaddonfeatureinfoOutput outputobj = new UtmsgetplanaddonfeatureinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}