﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using static UTMSDocker.Models.utmsplanpricinggetnotesinfoModel;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class utmsplanpricinggetnotesinfoDB
    { 

        public static List<utmsplanpricinggetnotesinfoOutput> CallDB(utmsplanpricinggetnotesinfoInput req,IConfiguration _Config,ILogger _logger)
        {
            List<utmsplanpricinggetnotesinfoOutput> OutputList = new List<utmsplanpricinggetnotesinfoOutput>();
            _logger.LogInformation("Input : utmsplanpricinggetnotesinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
          
                    var sp = "utms_plan_pricing_get_notes_info";
                object param = new
                {
                    @plan_id = req.plan_id,
                    @plan_notes_id = req.plan_notes_id
                };
                    IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); 
                if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new utmsplanpricinggetnotesinfoOutput()
                        {
                            plan_notes_id = r.plan_notes_id == null ? 0 : r.plan_notes_id,
                            plan_id = r.plan_id == null ? 0 : r.plan_id,
                            plan_notes = r.plan_notes,
                            plan_notes_status = r.plan_notes_status == null ? 0 : r.plan_notes_status,
                            create_date = r.create_date == null ? null : r.create_date,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        utmsplanpricinggetnotesinfoOutput outputobj = new utmsplanpricinggetnotesinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                utmsplanpricinggetnotesinfoOutput outputobj = new utmsplanpricinggetnotesinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}