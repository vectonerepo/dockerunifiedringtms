﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using UTMSDocker.Controllers;
using static UTMSDocker.Models.UtmsportalgetbundlelistinfoModel;

namespace UTMSDocker.DB
{
    public class UtmsportalgetbundlelistinfoDB
    {

        public static List<UtmsportalgetbundlelistinfoOutput> CallDB(UtmsportalgetbundlelistinfoInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UtmsportalgetbundlelistinfoOutput> OutputList = new List<UtmsportalgetbundlelistinfoOutput>();
            _logger.LogInformation("Input : UtmsportalgetbundlelistinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                // using (var conn = new SqlConnection(_Config["ConnectionStrings:UTMSAPP"]))
                //{
                // conn.Open();
                var sp = "utms_portal_get_bundle_list_info";
                object param = new

                {
                    @bundleid = req.bundleid

                };
                IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
                {
                    _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                    OutputList.AddRange(result.Select(r => new UtmsportalgetbundlelistinfoOutput()
                    {
                        id = r.id == null ? 0 : r.id,
                        bundleid = r.bundleid == null ? 0 : r.bundleid,
                        bundle_name = r.bundle_name,
                        price = r.price == null ? 0.0D : r.price,
                        national_min = r.national_min == null ? 0.0D : r.national_min,
                        international_min = r.international_min == null ? 0.0D : r.international_min,
                        bundle_status = r.bundle_status == null ? 0 : r.bundle_status,
                        bundle_type = r.bundle_type == null ? 0 : r.bundle_type,
                        Reseller_flag = r.Reseller_flag == null ? 0 : r.Reseller_flag,
                        days_valid = r.days_valid == null ? 0 : r.days_valid,
                        renewal_mode = r.renewal_mode == null ? 0 : r.renewal_mode,
                        renewal_status = r.renewal_status,
                        tariffclass = r.tariffclass,
                        sms_tariff = r.sms_tariff,
                        reset_mode = r.reset_mode == null ? 0 : r.reset_mode,
                        sms_allowance = r.sms_allowance == null ? 0.0F : r.sms_allowance,
                        Schedule_Type = r.Schedule_Type == null ? 0 : r.Schedule_Type,
                        schedule_time = r.schedule_time == null ? DateTime.MinValue : r.schedule_time,
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg
                    }));
                }
                else
                {
                    _logger.LogInformation("Output DB : " + "Empty result");
                    UtmsportalgetbundlelistinfoOutput outputobj = new UtmsportalgetbundlelistinfoOutput();
                    outputobj.Code = -1;
                    outputobj.Message = "No Rec found";
                    OutputList.Add(outputobj);
                }
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UtmsportalgetbundlelistinfoOutput outputobj = new UtmsportalgetbundlelistinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}