﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UTMSDocker.Controllers;
using static UTMSDocker.Models.UTMSgetplaninfoModel;

namespace UTMSDocker.DB
{
    public class UTMSgetplaninfoDB
    {


        public static List<UTMSgetplaninfoOutput> CallDB(UTMSgetplaninfoInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UTMSgetplaninfoOutput> OutputList = new List<UTMSgetplaninfoOutput>();

            var sp = "UTMS_get_plan_info";
            object param = new

            {
                @Product_ID = req.Product_ID,
                @Plan_id = req.Plan_id,
            };
            IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
            {
                OutputList.AddRange(result.Select(r => new UTMSgetplaninfoOutput()
                {
                    PC_Idx = r.PC_Idx == null ? 0 : r.PC_Idx,
                    Bundle_Type = r.Bundle_Type,
                    plan_name = r.plan_name,
                    PC_Price = r.PC_Price == null ? 0.0D : r.PC_Price,
                    duration = r.duration == null ? 0 : r.duration,
                    Request_ID = r.Request_ID == null ? 0 : r.Request_ID,
                    features = r.features,
                    National_min = r.National_min == null ? 0 : r.National_min,
                    Geo_Min = r.Geo_Min == null ? 0 : r.Geo_Min,
                    Unlimted_Intra_Call = r.Unlimted_Intra_Call == null ? 0 : r.Unlimted_Intra_Call,
                    is_free_trial = r.is_free_trial == null ? 0 : r.is_free_trial,
                    Code = r.errcode == null ? 0 : r.errcode,
                    Message = r.errmsg == null ? "Success" : r.errmsg
                }));
            }
            else
            {
                UTMSgetplaninfoOutput outputobj = new UTMSgetplaninfoOutput();
                outputobj.Code = 1003;
                outputobj.Message = ErrorMessage.Errors[1003];
                OutputList.Add(outputobj);
            }
            _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(OutputList));
            return OutputList;

        }
    }
}