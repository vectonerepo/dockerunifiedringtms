﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using static UTMSDocker.Models.UtmscreatescreenmoduleModel;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UtmscreatescreenmoduleDB
    {


        public static List<UtmscreatescreenmoduleOutput> CallDB(UtmscreatescreenmoduleInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UtmscreatescreenmoduleOutput> OutputList = new List<UtmscreatescreenmoduleOutput>();
            _logger.LogInformation("Input : UtmscreatescreenmoduleController:" + JsonConvert.SerializeObject(req));
            try
            {

                var sp = "UTMS_create_screen_Module";
                object param = new

                {
                    @Module_name = req.Module_name,
                    @Url = req.Url,
                    @Status = req.Status,
                    @Module_Id = req.Module_Id,
                    @processtype = req.processtype
                };
                IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
                {
                    _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                    OutputList.AddRange(result.Select(r => new UtmscreatescreenmoduleOutput()
                    {
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg
                    }));
                }
                else
                {
                    _logger.LogInformation("Output DB : " + "Empty result");
                    UtmscreatescreenmoduleOutput outputobj = new UtmscreatescreenmoduleOutput();
                    outputobj.Code = -1;
                    outputobj.Message = "No Rec found";
                    OutputList.Add(outputobj);
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UtmscreatescreenmoduleOutput outputobj = new UtmscreatescreenmoduleOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}