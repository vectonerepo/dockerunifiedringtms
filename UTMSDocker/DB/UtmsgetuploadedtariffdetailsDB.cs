﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UTMSDocker.Controllers;
using static UTMSDocker.Models.UtmsgetuploadedtariffdetailsModel;

namespace UTMSDocker.DB
{
    public class UtmsgetuploadedtariffdetailsDB
    {

        public static List<UtmsgetuploadedtariffdetailsOutput> CallDB(IConfiguration _Config, ILogger _logger)
        {
            List<UtmsgetuploadedtariffdetailsOutput> OutputList = new List<UtmsgetuploadedtariffdetailsOutput>();
            var sp = "utms_get_uploaded_tariff_details";
            object param = new
            {

            };
            IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
            {
                OutputList.AddRange(result.Select(r => new UtmsgetuploadedtariffdetailsOutput()
                {
                    trffclass = r.trffclass,
                    access_type = r.access_type == null ? 0 : r.access_type,
                    access_type_name = r.access_type_name,
                    plan_name = r.plan_name == null ? null : r.plan_name,
                    Code = r.errcode == null ? 0 : r.errcode,
                    Message = r.errmsg == null ? "Success" : r.errmsg,
                }));
            }
            else
            {
                UtmsgetuploadedtariffdetailsOutput outputobj = new UtmsgetuploadedtariffdetailsOutput();
                outputobj.Code = 1003;
                outputobj.Message = ErrorMessage.Errors[1003];
                OutputList.Add(outputobj);
            }
            _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(OutputList));
            return OutputList;
        }
    }
}