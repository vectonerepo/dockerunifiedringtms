﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UTMSDocker.Controllers;
using static UTMSDocker.Models.UTMSgetdiscountpercentagesettingsinfoModel;

namespace UTMSDocker.DB
{
    public class UTMSgetdiscountpercentagesettingsinfoDB
    {

        public static List<UTMSgetdiscountpercentagesettingsinfoOutput> CallDB(IConfiguration _Config, ILogger _logger)
        {
            List<UTMSgetdiscountpercentagesettingsinfoOutput> OutputList = new List<UTMSgetdiscountpercentagesettingsinfoOutput>();


            var sp = "UTMS_get_discount_percentage_settings_info";
            object param = new
            {
            };
            IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
            {
                OutputList.AddRange(result.Select(r => new UTMSgetdiscountpercentagesettingsinfoOutput()
                {
                    discount_percentage_id = r.discount_percentage_id == null ? 0 : r.discount_percentage_id,
                    discount_percentage_status = r.discount_percentage_status,
                    discount_percentage = r.discount_percentage == null ? 0.0F : r.discount_percentage,
                    max_no_of_user = r.max_no_of_user == null ? 0 : r.max_no_of_user,
                    Code = r.errcode == null ? 0 : r.errcode,
                    Message = r.errmsg == null ? "Success" : r.errmsg,
                }));
            }
            else
            {
                UTMSgetdiscountpercentagesettingsinfoOutput outputobj = new UTMSgetdiscountpercentagesettingsinfoOutput();
                outputobj.Code = 1003;
                outputobj.Message = ErrorMessage.Errors[1003];
                OutputList.Add(outputobj);
            }
            _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(OutputList));
            return OutputList;
        }
    }
}