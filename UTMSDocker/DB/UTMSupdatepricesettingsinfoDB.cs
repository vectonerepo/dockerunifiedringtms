﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using static UTMSDocker.Models.UTMSupdatepricesettingsinfoModel;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UTMSupdatepricesettingsinfoDB
    {


        public static List<UTMSupdatepricesettingsinfoOutput> CallDB(UTMSupdatepricesettingsinfoInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UTMSupdatepricesettingsinfoOutput> OutputList = new List<UTMSupdatepricesettingsinfoOutput>();
            _logger.LogInformation("Input : UTMSupdatepricesettingsinfoController:" + JsonConvert.SerializeObject(req));
            try
            { 
                var sp = "UTMS_update_price_settings_info";
                object param = new
                {
                    @pricing_id = req.pricing_id,
                    @pricing_status = req.pricing_status
                };
                IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP");
                if (result != null && result.Count() > 0)
                {
                    _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                    OutputList.AddRange(result.Select(r => new UTMSupdatepricesettingsinfoOutput()
                    {
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg
                    }));
                }
                else
                {
                    _logger.LogInformation("Output DB : " + "Empty result");
                    UTMSupdatepricesettingsinfoOutput outputobj = new UTMSupdatepricesettingsinfoOutput();
                    outputobj.Code = -1;
                    outputobj.Message = "No Rec found";
                    OutputList.Add(outputobj);
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UTMSupdatepricesettingsinfoOutput outputobj = new UTMSupdatepricesettingsinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}