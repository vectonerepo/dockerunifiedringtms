﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using static UTMSDocker.Models.UrtariffgettariffdetailsModel;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UrtariffgettariffdetailsDB
    {


        public static List<UrtariffgettariffdetailsOutput> CallDB(UrtariffgettariffdetailsInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UrtariffgettariffdetailsOutput> OutputList = new List<UrtariffgettariffdetailsOutput>();
            _logger.LogInformation("Input : UrtariffgettariffdetailsController:" + JsonConvert.SerializeObject(req));
            try
            {

                var sp = "ur_tariff_get_tariff_details";
                object param = new

                {
                    @id = req.id,
                    @planid = req.planid
                };
                IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
                {
                    _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                    OutputList.AddRange(result.Select(r => new UrtariffgettariffdetailsOutput()
                    {
                        Plan_id = r.Plan_id == null ? 0 : r.Plan_id,
                        Pln_Name = r.Pln_Name,
                        trffclass = r.trffclass,
                        trf_type = r.trf_type == null ? 0 : r.trf_type,
                        plan_status = r.plan_status,
                        id = r.id == null ? 0 : r.id,
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg
                    }));
                }
                else
                {
                    _logger.LogInformation("Output DB : " + "Empty result");
                    UrtariffgettariffdetailsOutput outputobj = new UrtariffgettariffdetailsOutput();
                    outputobj.Code = -1;
                    outputobj.Message = "No Rec found";
                    OutputList.Add(outputobj);
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UrtariffgettariffdetailsOutput outputobj = new UrtariffgettariffdetailsOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}