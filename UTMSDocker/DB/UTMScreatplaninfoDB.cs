﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using static UTMSDocker.Models.UTMScreateplaninfoModel;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UTMScreateplaninfoDB
    {
         

        public static List<UTMScreateplaninfoOutput> CallDB(UTMScreateplaninfoInput req,IConfiguration _Config,ILogger _logger)
        {
            List<UTMScreateplaninfoOutput> OutputList = new List<UTMScreateplaninfoOutput>();
            _logger.LogInformation("Input : UtmscreatemenuinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
               
                    var sp = "UTMS_create_plan_info";
                object param = new

                {


                    @User_Id = req.User_Id,
                    @Product_Id = req.Product_Id,
                    @duration = req.duration,
                    @Pc_From_Users = req.Pc_From_Users,
                    @Pc_To_Users = req.Pc_To_Users,
                    @PC_Price = req.PC_Price,
                    @features = req.features,
                    @Process_Type = req.Process_Type,
                    @Plan_ID = req.Plan_ID,
                    @National_min = req.National_min,
                    @Geo_Min = req.Geo_Min,
                    @Plan_Name = req.Plan_Name,
                    @Unlimted_Intra_Call = req.Unlimted_Intra_Call

                };
                    IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP");  if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UTMScreateplaninfoOutput()
                        {
                            is_free_trial =r.is_free_trial == null ? 0 : r.is_free_trial,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        UTMScreateplaninfoOutput outputobj = new UTMScreateplaninfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
               
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UTMScreateplaninfoOutput outputobj = new UTMScreateplaninfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }


    }
}