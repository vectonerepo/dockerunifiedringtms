﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using static UTMSDocker.Models.UtmscreatemenuinfoModel;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UtmscreatemenuinfoDB
    {


        public static List<UtmscreatemenuinfoOutput> CallDB(UtmscreatemenuinfoInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UtmscreatemenuinfoOutput> OutputList = new List<UtmscreatemenuinfoOutput>();
            _logger.LogInformation("Input : UtmscreatemenuinfoController:" + JsonConvert.SerializeObject(req));
            try
            {

                var sp = "UTMS_create_Menu_Info";
                object param = new
                {
                    @Menu_Name = req.Menu_Name,
                    @Menu_link = req.Menu_link,
                    @process_type = req.process_type,
                    @Menu_Id = req.Menu_Id
                };
                IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
                {
                    _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                    OutputList.AddRange(result.Select(r => new UtmscreatemenuinfoOutput()
                    {
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg
                    }));
                }
                else
                {
                    _logger.LogInformation("Output DB : " + "Empty result");
                    UtmscreatemenuinfoOutput outputobj = new UtmscreatemenuinfoOutput();
                    outputobj.Code = -1;
                    outputobj.Message = "No Rec found";
                    OutputList.Add(outputobj);
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UtmscreatemenuinfoOutput outputobj = new UtmscreatemenuinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}