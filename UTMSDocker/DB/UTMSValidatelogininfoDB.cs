﻿using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using static UTMSDocker.Models.UTMSValidatelogininfoModel;
using Dapper;
using System.Data;
using UTMSDocker.Controllers;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UTMSValidatelogininfoDB
    {


        public static List<UTMSValidatelogininfoOutput> CallDB(UTMSValidatelogininfoInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UTMSValidatelogininfoOutput> OutputList = new List<UTMSValidatelogininfoOutput>();
            _logger.LogInformation("Input : UtmsgetuserprofileinfoController:" + JsonConvert.SerializeObject(req));
            try
            { 
                var sp = "UTMS_Validate_login_info ";
                object param = new
                {
                    @Username = req.Username,
                    @Password = req.Password,
                    @Last_login_Ip = req.Last_login_Ip
                };
                IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP");
                if (result != null && result.Count() > 0)
                {
                    _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                    OutputList.AddRange(result.Select(r => new UTMSValidatelogininfoOutput()
                    {
                        UserId = r.UserId == null ? 0 : r.UserId,
                        role_id = r.role_id == null ? 0 : r.role_id,
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg
                    }));
                }
                else
                {
                    _logger.LogInformation("Output DB : " + "Empty result");
                    UTMSValidatelogininfoOutput outputobj = new UTMSValidatelogininfoOutput();
                    outputobj.Code = 1003;
                    outputobj.Message = ErrorMessage.Errors[1003];
                    OutputList.Add(outputobj);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UTMSValidatelogininfoOutput outputobj = new UTMSValidatelogininfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}