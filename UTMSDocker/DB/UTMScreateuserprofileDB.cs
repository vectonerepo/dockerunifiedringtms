﻿using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using static UTMSDocker.Models.UTMScreateuserprofileModel;
using Dapper;
using System.Data;
using UTMSDocker.Controllers;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UTMScreateuserprofileDB
    {

         

        public static List<UTMScreateuserprofileOutput> CallDB(UTMScreateuserprofileInput req,IConfiguration _Config,ILogger _logger)
        {
            List<UTMScreateuserprofileOutput> OutputList = new List<UTMScreateuserprofileOutput>();
            _logger.LogInformation("Input : UTMScreateuserprofileDBController:" + JsonConvert.SerializeObject(req));
            try
            {
                
                    var sp = "UTMS_create_user_profile";
                object param = new

                {
                    @First_name = req.First_name,
                    @Last_name = req.Last_name,
                    @Username = req.Username,
                    @Password = req.Password,
                    @role_id = req.role_id,
                    @last_login_ip = req.last_login_ip,
                    @Status = req.Status,
                    @Menu_id = req.Menu_id,
                    @process_Type = req.process_Type,
                    @user_id = req.user_id
                };
                    IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP");  if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UTMScreateuserprofileOutput()
                        {
                            //Userid = r.Userid == null ? 0 : r.Userid,
                            //First_name = r.First_name,
                            //Last_name = r.Last_name,
                            //Username = r.Username,
                            //Password = r.Password,
                            //Fk_role_Id = r.Fk_role_Id == null ? 0 : r.errcode,
                            //last_login_ip = r.last_login_ip,
                            //Role_name = r.Role_name,
                            //User_status = r.User_status,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        UTMScreateuserprofileOutput outputobj = new UTMScreateuserprofileOutput();
                        outputobj.Code = 1003;
                        outputobj.Message = ErrorMessage.Errors[1003];
                        OutputList.Add(outputobj);
                    }
                
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UTMScreateuserprofileOutput outputobj = new UTMScreateuserprofileOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}