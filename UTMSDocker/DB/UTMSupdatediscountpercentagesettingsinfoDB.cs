﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using static UTMSDocker.Models.UTMSupdatediscountpercentagesettingsinfoModel;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UTMSupdatediscountpercentagesettingsinfoDB
    { 

        public static List<UTMSupdatediscountpercentagesettingsinfoOutput> CallDB(UTMSupdatediscountpercentagesettingsinfoInput req,IConfiguration _Config,ILogger _logger) 
        {
            List<UTMSupdatediscountpercentagesettingsinfoOutput> OutputList = new List<UTMSupdatediscountpercentagesettingsinfoOutput>();
            _logger.LogInformation("Input : UTMSupdaUTMSupdatediscountpercentagesettingsinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
               
                    var sp = "UTMS_update_discount_percentage_settings_info";
                    object param = new
                    {
                        @discount_percentage_id = req.discount_percentage_id,
                        @discount_percentage_status = req.discount_percentage_status,
                        @discount_percentage = req.discount_percentage,
                        @max_no_of_user = req.max_no_of_user
                    };
                    IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP");  if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UTMSupdatediscountpercentagesettingsinfoOutput()
                        {
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        UTMSupdatediscountpercentagesettingsinfoOutput outputobj = new UTMSupdatediscountpercentagesettingsinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    } 
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UTMSupdatediscountpercentagesettingsinfoOutput outputobj = new UTMSupdatediscountpercentagesettingsinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}