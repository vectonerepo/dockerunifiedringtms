﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using static UTMSDocker.Models.UtmsgetfeaturedetailsbyplanModel;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UtmsgetfeaturedetailsbyplanDB
    {

         
        public static List<UtmsgetfeaturedetailsbyplanOutput> CallDB(UtmsgetfeaturedetailsbyplanInput req,IConfiguration _Config,ILogger _logger)
        {
            List<UtmsgetfeaturedetailsbyplanOutput> OutputList = new List<UtmsgetfeaturedetailsbyplanOutput>();
            _logger.LogInformation("Input : UtmsgetfeaturedetailsbyplanController:" + JsonConvert.SerializeObject(req));
            try
            {
            
                    var sp = "utms_get_feature_details_by_plan";
                    object param = new
                    {
                        @plan_id = req.plan_id
                    };
                    IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP");  if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UtmsgetfeaturedetailsbyplanOutput()
                        {
                            PFD_Feature = r.PFD_Feature == null ? 0 : r.PFD_Feature,
                            //price = r.price == null ? 0.0D : r.price,
                            FT_Keyword = r.FT_Keyword,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        UtmsgetfeaturedetailsbyplanOutput outputobj = new UtmsgetfeaturedetailsbyplanOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    } 
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UtmsgetfeaturedetailsbyplanOutput outputobj = new UtmsgetfeaturedetailsbyplanOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}