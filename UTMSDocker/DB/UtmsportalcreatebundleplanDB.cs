﻿using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using static UTMSDocker.Models.UtmsportalcreatebundleplanModel;
using Dapper;
using System.Data;
using UTMSDocker.Controllers;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UtmsportalcreatebundleplanDB
    {


        public static List<UtmsportalcreatebundleplanOutput> CallDB(UtmsportalcreatebundleplanInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UtmsportalcreatebundleplanOutput> OutputList = new List<UtmsportalcreatebundleplanOutput>();
            _logger.LogInformation("Input : UtmsportalcreatebundleplanController:" + JsonConvert.SerializeObject(req));
            try
            {
                // using (var conn = new SqlConnection(_Config["ConnectionStrings:UTMSAPP"]))
                //{
                // conn.Open();
                var sp = "utms_portal_create_bundle_plan";
                object param = new

                {
                    @sitecode = req.sitecode,
                    @bundle_name = req.bundle_name,
                    @currency = req.currency,
                    @price = req.price,
                    @tariffclass_groupid = req.tariffclass_groupid,
                    @renewal_delay = req.renewal_delay,
                    @renewal_mode = req.renewal_mode,
                    @pro_rata = req.pro_rata,
                    @national_min = req.national_min,
                    @international_min = req.international_min,
                    @bundle_type = req.bundle_type,
                    @reseller_flag = req.reseller_flag
                };
                IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
                {
                    _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                    OutputList.AddRange(result.Select(r => new UtmsportalcreatebundleplanOutput()
                    {
                        sitecode = r.sitecode,
                        currency = r.currency,
                        price = r.price == null ? 0.0D : r.price,
                        tariffclass_groupid = r.tariffclass_groupid == null ? 0 : r.tariffclass_groupid,
                        renewal_delay = r.renewal_delay == null ? 0 : r.renewal_delay,
                        renewal_mode = r.renewal_mode == null ? 0 : r.renewal_mode,
                        pro_rata = r.pro_rata == null ? 0 : r.pro_rata,
                        national_min = r.national_min == null ? 0.0D : r.national_min,
                        international_min = r.international_min == null ? 0.0D : r.international_min,
                        bundleid = r.bundleid == null ? 0 : r.bundleid,
                        bundle_name = r.bundle_name,
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg
                    }));
                }
                else
                {
                    _logger.LogInformation("Output DB : " + "Empty result");
                    UtmsportalcreatebundleplanOutput outputobj = new UtmsportalcreatebundleplanOutput();
                    outputobj.Code = 1003;
                    outputobj.Message = ErrorMessage.Errors[1003];
                    OutputList.Add(outputobj);
                }
                // }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UtmsportalcreatebundleplanOutput outputobj = new UtmsportalcreatebundleplanOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}