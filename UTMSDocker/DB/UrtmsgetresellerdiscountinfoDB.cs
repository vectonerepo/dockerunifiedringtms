﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json; 
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using static UTMSDocker.UrtmsgetresellerdiscountinfoController;


namespace UTMSDocker
{
    class UrtmsgetresellerdiscountinfoDB
    { 

        public static List<UrtmsgetresellerdiscountinfoOutput> CallDB(UrtmsgetresellerdiscountinfoInput req,IConfiguration _Config,ILogger _logger)
        {
            List<UrtmsgetresellerdiscountinfoOutput> OutputList = new List<UrtmsgetresellerdiscountinfoOutput>();            

           _logger.LogInformation("Input : UrtmsgetresellerdiscountinfoDB :" + JsonConvert.SerializeObject(req));

            try
            {
               
                var sp = "ur_tms_get_reseller_discount_info";
                object param = new

                {
                    @product_id = req.product_id
                };
                    IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); 
                if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrtmsgetresellerdiscountinfoOutput()
                        {
                            user_range_from = r.user_range_from == null ? 0 : r.user_range_from,
                            user_range_to = r.user_range_to == null ? 0 : r.user_range_to,
                            discount = r.discount == null ? 0.0F : r.discount,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        UrtmsgetresellerdiscountinfoOutput outputobj = new UrtmsgetresellerdiscountinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
            }
            catch (Exception ex)
            {
                //_logger.LogError(ex.Message);
                UrtmsgetresellerdiscountinfoOutput outputobj = new UrtmsgetresellerdiscountinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}