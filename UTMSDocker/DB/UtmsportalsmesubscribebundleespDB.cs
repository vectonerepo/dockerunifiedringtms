﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using UTMSDocker.Controllers;
using static UTMSDocker.Models.UtmsportalsmesubscribebundleespModel;

namespace UTMSDocker.DB
{
    public class UtmsportalsmesubscribebundleespDB
    {


        public static List<UtmsportalsmesubscribebundleespOutput> CallDB(UtmsportalsmesubscribebundleespInput req, IConfiguration _Config, ILogger _logger)
        {

            List<UtmsportalsmesubscribebundleespOutput> OutputList = new List<UtmsportalsmesubscribebundleespOutput>();
            _logger.LogInformation("Input : UtmsportalsmesubscribebundleespController:" + JsonConvert.SerializeObject(req));
            try
            {
                // using (var conn = new SqlConnection(_Config["ConnectionStrings:UTMSAPP"]))
                //{
                // conn.Open();
                var sp = "utms_portal_sme_subscribe_bundle_esp";
                object param = new

                {
                    @company_id = req.company_id,
                    @sitecode = req.sitecode,
                    @enterpriseid = req.enterpriseid,
                    @bundleid = req.bundleid,
                    @paymode = req.paymode,
                    @processby = req.processby

                };
                IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
                {
                    _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                    OutputList.AddRange(result.Select(r => new UtmsportalsmesubscribebundleespOutput()
                    {
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg
                    }));
                }
                else
                {
                    _logger.LogInformation("Output DB : " + "Empty result");
                    UtmsportalsmesubscribebundleespOutput outputobj = new UtmsportalsmesubscribebundleespOutput();
                    outputobj.Code = 1003;
                    outputobj.Message = ErrorMessage.Errors[1003];
                    OutputList.Add(outputobj);
                }
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UtmsportalsmesubscribebundleespOutput outputobj = new UtmsportalsmesubscribebundleespOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}