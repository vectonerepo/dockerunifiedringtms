﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using static UTMSDocker.Models.UtmscompanybulkuploadcallingratedtlinfoModel;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UtmscompanybulkuploadcallingratedtlinfoDB
    {
         

        public static List<UtmscompanybulkuploadcallingratedtlinfoOutput> CallDB(UtmscompanybulkuploadcallingratedtlinfoInput req,IConfiguration _Config,ILogger _logger)
        {
            List<UtmscompanybulkuploadcallingratedtlinfoOutput> OutputList = new List<UtmscompanybulkuploadcallingratedtlinfoOutput>();
            _logger.LogInformation("Input : UtmscompanybulkuploadcallingratedtlinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:UTMSAPP"]))
                //{
                   // conn.Open();
                    var sp = "utms_company_bulk_upload_calling_rate_dtl_info";
                    object param = new 
                    {
                        @processtype = req.processtype,
                        @in_xml_data = req.in_xml_data

                    };
                    IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP");  
                if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UtmscompanybulkuploadcallingratedtlinfoOutput()
                        {
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));

                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        UtmscompanybulkuploadcallingratedtlinfoOutput outputobj = new UtmscompanybulkuploadcallingratedtlinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UtmscompanybulkuploadcallingratedtlinfoOutput outputobj = new UtmscompanybulkuploadcallingratedtlinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}