﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UTMSDocker.Controllers;
using static UTMSDocker.Models.UtmscompanygetcallingtypeinfoModel;

namespace UTMSDocker.DB
{
    public class UtmscompanygetcallingtypeinfoDB
    {

        public static List<UtmscompanygetcallingtypeinfoOutput> CallDB(IConfiguration _Config, ILogger _logger)
        {
            List<UtmscompanygetcallingtypeinfoOutput> OutputList = new List<UtmscompanygetcallingtypeinfoOutput>();

            var sp = "utms_company_get_calling_type_info";
            object param = new
            {
            };
            IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
            {
                OutputList.AddRange(result.Select(r => new UtmscompanygetcallingtypeinfoOutput()
                {
                    calling_id = r.calling_id == null ? 0 : r.calling_id,
                    calling_type = r.calling_type,
                    status = r.status == null ? 0 : r.status,
                    Code = r.errcode == null ? 0 : r.errcode,
                    Message = r.errmsg == null ? "Success" : r.errmsg,
                }));
            }
            else
            {
                UtmscompanygetcallingtypeinfoOutput outputobj = new UtmscompanygetcallingtypeinfoOutput();
                outputobj.Code = 1003;
                outputobj.Message = ErrorMessage.Errors[1003];
                OutputList.Add(outputobj);
            }
            _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(OutputList));
            return OutputList;

        }
    }
}