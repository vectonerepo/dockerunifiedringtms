﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UTMSDocker.Controllers;
using static UTMSDocker.Models.UTMSgetCategoryinfoModel;


namespace UTMSDocker.DB
{
    public class UTMSgetCategoryinfoDB
    {

        public static List<UTMSgetCategoryinfoOutput> CallDB(UTMSgetCategoryinfoInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UTMSgetCategoryinfoOutput> OutputList = new List<UTMSgetCategoryinfoOutput>();

            var sp = "UTMS_get_Category_info";
            object param = new

            {
                @Category_id = req.Category_id
            };
            IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
            {
                OutputList.AddRange(result.Select(r => new UTMSgetCategoryinfoOutput()
                {
                    Category_id = r.Category_id == null ? 0 : r.Category_id,
                    Category_Name = r.Category_Name,
                    category_status = r.category_status,
                    Code = r.errcode == null ? 0 : r.errcode,
                    Message = r.errmsg == null ? "Success" : r.errmsg,
                    category_position = r.category_position == null ? 0 : r.category_position
                }));
            }
            else
            {
                UTMSgetCategoryinfoOutput outputobj = new UTMSgetCategoryinfoOutput();
                outputobj.Code = 1003;
                outputobj.Message = ErrorMessage.Errors[1003];
                OutputList.Add(outputobj);
            }
            _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(OutputList));
            return OutputList;

        }
    }
}