﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UTMSDocker.Controllers;
using static UTMSDocker.Models.UTMSgetFreetrialsettingsinfoModel;

namespace UTMSDocker.DB
{
    public class UTMSgetFreetrialsettingsinfoDB
    {


        public static List<UTMSgetFreetrialsettingsinfoOutput> CallDB(IConfiguration _Config, ILogger _logger)
        {
            List<UTMSgetFreetrialsettingsinfoOutput> OutputList = new List<UTMSgetFreetrialsettingsinfoOutput>(); 
            var sp = "UTMS_get_Free_trial_settings_info";
            object param = new
            {
            };
            IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
            {
                OutputList.AddRange(result.Select(r => new UTMSgetFreetrialsettingsinfoOutput()
                {
                    Free_trial_id = r.Free_trial_id == null ? 0 : r.Free_trial_id,
                    Free_trial_day = r.Free_trial_day == null ? 0 : r.Free_trial_day,
                    Free_trial_status = r.Free_trial_status,
                    Code = r.errcode == null ? 0 : r.errcode,
                    Message = r.errmsg == null ? "Success" : r.errmsg,
                }));
            }
            else
            {
                UTMSgetFreetrialsettingsinfoOutput outputobj = new UTMSgetFreetrialsettingsinfoOutput();
                outputobj.Code = 1003;
                outputobj.Message = ErrorMessage.Errors[1003];
                OutputList.Add(outputobj);
            }
            _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(OutputList));
            return OutputList; 
        }
    }
}