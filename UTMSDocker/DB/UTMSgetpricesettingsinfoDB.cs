﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UTMSDocker.Controllers;
using static UTMSDocker.Models.UTMSgetpricesettingsinfoModel;
namespace UTMSDocker.DB
{
    public class UTMSgetpricesettingsinfoDB
    {



        public static List<UTMSgetpricesettingsinfoOutput> CallDB(IConfiguration _Config, ILogger _logger)
        {
            List<UTMSgetpricesettingsinfoOutput> OutputList = new List<UTMSgetpricesettingsinfoOutput>();


            var sp = "UTMS_get_price_settings_info";
            object param = new
            {
            };
            IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
            {
                OutputList.AddRange(result.Select(r => new UTMSgetpricesettingsinfoOutput()
                {

                    pricing_id = r.pricing_id == null ? 0 : r.pricing_id,
                    pricing_status = r.pricing_status,
                    Code = r.errcode == null ? 0 : r.errcode,
                    Message = r.errmsg == null ? "Success" : r.errmsg,
                }));
            }
            else
            {
                UTMSgetpricesettingsinfoOutput outputobj = new UTMSgetpricesettingsinfoOutput();
                outputobj.Code = 1003;
                outputobj.Message = ErrorMessage.Errors[1003];
                OutputList.Add(outputobj);
            }
            _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(OutputList));
            return OutputList;

        }
    }
}