﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UTMSDocker.Controllers;
using static UTMSDocker.Models.UTMSgetuserrangemasterdetailModel;

namespace UTMSDocker.DB
{
    public class UTMSgetuserrangemasterdetailDB
    {
         
        //public static List<UTMSgetuserrangemasterdetailOutput> CallDB()
            public static List<UTMSgetuserrangemasterdetailOutput> CallDB(UTMSgetuserrangemasterdetailInput req,IConfiguration _Config,ILogger _logger)
        {
            List<UTMSgetuserrangemasterdetailOutput> OutputList = new List<UTMSgetuserrangemasterdetailOutput>();
                var sp = "UTMS_get_user_range_master_detail";
            object param = new
            {
                @Range_typeid = req.Range_typeid,
                @plan_id = req.plan_id
            };
                IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP");  if (result != null && result.Count() > 0)
                {
                    OutputList.AddRange(result.Select(r => new UTMSgetuserrangemasterdetailOutput()
                    {
                        min_user = r.min_user == null ? 0 : r.min_user,
                        Range_typeid = r.Range_typeid == null ? 0 : r.Range_typeid,
                        From_user_Range = r.From_user_Range == null ? 0 : r.From_user_Range,
                        To_user_Range = r.To_user_Range == null ? 0 : r.To_user_Range,
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg,
                    }));
                }
                else
                {
                    UTMSgetuserrangemasterdetailOutput outputobj = new UTMSgetuserrangemasterdetailOutput();
                    outputobj.Code = 1003;
                    outputobj.Message = ErrorMessage.Errors[1003];
                    OutputList.Add(outputobj);
                }
                _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(OutputList));
                return OutputList;
            //}
        }
    }
}