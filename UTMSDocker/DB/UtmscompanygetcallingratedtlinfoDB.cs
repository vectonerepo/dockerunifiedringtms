﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using static UTMSDocker.Models.UtmscompanygetcallingratedtlinfoModel;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UtmscompanygetcallingratedtlinfoDB
    {
         

        public static List<UtmscompanygetcallingratedtlinfoOutput> CallDB(UtmscompanygetcallingratedtlinfoInput req,IConfiguration _Config,ILogger _logger)
        {
            List<UtmscompanygetcallingratedtlinfoOutput> OutputList = new List<UtmscompanygetcallingratedtlinfoOutput>();
            _logger.LogInformation("Input : UtmscompanygetcallingratedtlinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:UTMSAPP"]))
                //{
                   // conn.Open();
                    var sp = "utms_company_get_calling_rate_dtl_info";
                    object param = new
                    {
                        @id = req.id,
                        @calling_id = req.calling_id

                    };
                    IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP");  
                if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UtmscompanygetcallingratedtlinfoOutput()
                        {

                            id = r.id == null ? 0 : r.id,
                            calling_id = r.calling_id == null ? 0 : r.calling_id,
                            destination = r.destination,
                            type = r.type,
                            prefix = r.prefix,
                            call_rate = r.call_rate,
                            status = r.status == null ? 0 : r.status,
                            calling_type = r.calling_type,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));

    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        UtmscompanygetcallingratedtlinfoOutput outputobj = new UtmscompanygetcallingratedtlinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UtmscompanygetcallingratedtlinfoOutput outputobj = new UtmscompanygetcallingratedtlinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}