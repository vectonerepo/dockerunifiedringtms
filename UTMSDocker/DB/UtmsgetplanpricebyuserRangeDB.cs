﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UTMSDocker.Controllers;
using static UTMSDocker.Models.UTMSgetplanpricebyuserRangeModel;


namespace UTMSDocker.DB
{
    public class UTMSgetplanpricebyuserRangeDB
    {

        public static List<UTMSgetplanpricebyuserRangeOutput> CallDB(UTMSgetplanpricebyuserRangeInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UTMSgetplanpricebyuserRangeOutput> OutputList = new List<UTMSgetplanpricebyuserRangeOutput>();


            var sp = "Utms_get_plan_price_by_user_Range";
            object param = new

            {
                @plan_id = req.plan_id
            };
            IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
            {
                OutputList.AddRange(result.Select(r => new UTMSgetplanpricebyuserRangeOutput()
                {
                    PC_Plan = r.PC_Plan == null ? 0 : r.PC_Plan,
                    Duration = r.Duration == null ? 0 : r.Duration,
                    From_User = r.From_User == null ? 0 : r.From_User,
                    To_User = r.To_User == null ? 0 : r.To_User,
                    price = r.price == null ? 0.0D : r.price,
                    type_desc = r.type_desc,
                    plan_dur_flag_id = r.plan_dur_flag_id == null ? 0 : r.plan_dur_flag_id,
                    Code = r.errcode == null ? 0 : r.errcode,
                    Message = r.errmsg == null ? "Success" : r.errmsg,
                }));
            }
            else
            {
                UTMSgetplanpricebyuserRangeOutput outputobj = new UTMSgetplanpricebyuserRangeOutput();
                outputobj.Code = 1003;
                outputobj.Message = ErrorMessage.Errors[1003];
                OutputList.Add(outputobj);
            }
            _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(OutputList));
            return OutputList;

        }
    }
}