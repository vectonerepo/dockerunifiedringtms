﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using static UTMSDocker.Models.UtmscreateesptariffModel;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UtmscreateesptariffDB
    {

        public static List<UtmscreateesptariffOutput> CallDB(UtmscreateesptariffInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UtmscreateesptariffOutput> OutputList = new List<UtmscreateesptariffOutput>();
            _logger.LogInformation("Input : UtmscreateesptariffDB:" + JsonConvert.SerializeObject(req));
            try
            {
                var sp = "utms_create_esp_tariff";
                object param = new
                {
                    @plan_id = req.plan_id,
                    @tariff = req.tariff,
                    @tariff_type = req.tariff_type,
                    @processby = req.processby,
                    @processtype = req.processtype,
                    @id = req.id
                };
                IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
                {
                    _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                    OutputList.AddRange(result.Select(r => new UtmscreateesptariffOutput()
                    {
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg,
                    }));
                }
                else
                {
                    _logger.LogInformation("Output DB : " + "Empty result");
                    UtmscreateesptariffOutput outputobj = new UtmscreateesptariffOutput();
                    outputobj.Code = -1;
                    outputobj.Message = "No Rec found";
                    OutputList.Add(outputobj);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UtmscreateesptariffOutput outputobj = new UtmscreateesptariffOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}