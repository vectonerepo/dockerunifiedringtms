﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using static UTMSDocker.Models.UtmsgetwebratescountryprefixinfoModel;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UtmsgetwebratescountryprefixinfoDB
    {

        public static List<UtmsgetwebratescountryprefixinfoOutput> CallDB(UtmsgetwebratescountryprefixinfoInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UtmsgetwebratescountryprefixinfoOutput> OutputList = new List<UtmsgetwebratescountryprefixinfoOutput>();
            _logger.LogInformation("Input : UtmsgetwebratescountryprefixinfoController:" + JsonConvert.SerializeObject(req));
            try
            {

                var sp = "utms_get_web_rates_country_prefix_info";
                object param = new
                {
                    @callingtype = req.callingtype,
                    @type = req.type,
                    @country = req.country
                };

                IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP");
                if (result != null && result.Count() > 0)
                {
                    _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                    OutputList.AddRange(result.Select(r => new UtmsgetwebratescountryprefixinfoOutput()
                    {
                        calling_type = r.calling_type,
                        country = r.country,
                        prefix = r.prefix,
                        id = r.id == null ? 0 : r.id,
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg
                    }));
                }
                else
                {
                    _logger.LogInformation("Output DB : " + "Empty result");
                    UtmsgetwebratescountryprefixinfoOutput outputobj = new UtmsgetwebratescountryprefixinfoOutput();
                    outputobj.Code = -1;
                    outputobj.Message = "No Rec found";
                    OutputList.Add(outputobj);
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UtmsgetwebratescountryprefixinfoOutput outputobj = new UtmsgetwebratescountryprefixinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}