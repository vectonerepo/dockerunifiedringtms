﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using static UTMSDocker.Models.UtmsgetresellerdiscountinfoModel;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UtmsgetresellerdiscountinfoDB
    {


        public static List<UtmsgetresellerdiscountinfoOutput> CallDB(UtmsgetresellerdiscountinfoInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UtmsgetresellerdiscountinfoOutput> OutputList = new List<UtmsgetresellerdiscountinfoOutput>();
            _logger.LogInformation("Input : Utmsgetresellerdiscountinfo:" + JsonConvert.SerializeObject(req));
            try
            {

                var sp = "utms_get_reseller_discount_info";
                object param = new
                {
                    @pc_product = req.pc_product
                };
                IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
                {
                    _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                    OutputList.AddRange(result.Select(r => new UtmsgetresellerdiscountinfoOutput()
                    {
                        user_range_from = r.user_range_from == null ? 0 : r.user_range_from,
                        user_range_to = r.user_range_to == null ? 0 : r.user_range_to,
                        discount = r.discount == null ? 0.0D : r.discount,
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg
                    }));
                }
                else
                {
                    _logger.LogInformation("Output DB : " + "Empty result");
                    UtmsgetresellerdiscountinfoOutput outputobj = new UtmsgetresellerdiscountinfoOutput();
                    outputobj.Code = -1;
                    outputobj.Message = "No Rec found";
                    OutputList.Add(outputobj);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UtmsgetresellerdiscountinfoOutput outputobj = new UtmsgetresellerdiscountinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}