﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using NLog.Fluent;
using UTMSDocker.Controllers;
using static UTMSDocker.Models.UTMSGetMenuInfoModel;

namespace UTMSDocker.DB
{
    public class UTMSGetMenuInfoDB
    {

        public static List<UTMSGetMenuInfoOutput> CallDB(UTMSGetMenuInfoInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UTMSGetMenuInfoOutput> OutputList = new List<UTMSGetMenuInfoOutput>();


            var sp = "UTMS_get_Menu_Info";
            object param = new

            {
                @Menu_Id = req.Menu_Id
            };
            IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
            {
                OutputList.AddRange(result.Select(r => new UTMSGetMenuInfoOutput()
                {
                    Menu_Id = r.Menu_Id,
                    Menu_Name = r.Menu_Name,
                    Menu_link = r.Menu_link,
                    Code = r.errcode == null ? 0 : r.errcode,
                    Message = r.errmsg == null ? "Success" : r.errmsg
                }));
            }
            else
            {
                UTMSGetMenuInfoOutput outputobj = new UTMSGetMenuInfoOutput();
                outputobj.Code = 1003;
                outputobj.Message = ErrorMessage.Errors[1003];
                OutputList.Add(outputobj);
            }
            _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(OutputList));
            return OutputList;

        }
    }
}