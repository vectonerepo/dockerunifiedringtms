﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using static UTMSDocker.Models.UtmsgetwebsratescountrysummaryinfoModel;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UtmsgetwebsratescountrysummaryinfoDB
    {    
        public static List<UtmsgetwebsratescountrysummaryinfoOutput> CallDB(UtmsgetwebsratescountrysummaryinfoInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UtmsgetwebsratescountrysummaryinfoOutput> OutputList = new List<UtmsgetwebsratescountrysummaryinfoOutput>();
            _logger.LogInformation("Input : UtmsgetwebsratescountrysummaryinfoDB :" + JsonConvert.SerializeObject(req));
            try
            {               
                    var sp = "utms_get_web_rates_country_summary_info";
                    object param = new
                    {
                        @callingtype = req.callingtype
                    };
                    IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP");  if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UtmsgetwebsratescountrysummaryinfoOutput()
                        {
                            calling_type = r.calling_type,
                            Type = r.Type,
                            country = r.country,
                            prefix_count = r.prefix_count == null ? 0 : r.prefix_count,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg,


                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        UtmsgetwebsratescountrysummaryinfoOutput outputobj = new UtmsgetwebsratescountrysummaryinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                 
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UtmsgetwebsratescountrysummaryinfoOutput outputobj = new UtmsgetwebsratescountrysummaryinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}