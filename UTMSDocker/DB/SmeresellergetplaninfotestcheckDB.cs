﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using static UTMSDocker.Models.SmeresellergetplaninfotestcheckModel;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class SmeresellergetplaninfotestcheckDB
    {
         
        public static List<smeresellergetplaninfotestcheckOutput> CallDB(SmeresellergetplaninfotestcheckInput req,IConfiguration _Config,ILogger _logger)
        {
            List<smeresellergetplaninfotestcheckOutput> OutputList = new List<smeresellergetplaninfotestcheckOutput>();
            _logger.LogInformation("Input : SmeresellergetplaninfotestcheckDB:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:UTMSAPP"]))
                //{
                   // conn.Open();
                    //UTMS_create_plan_info_test_check
                    var sp = "sme_reseller_get_plan_info_testcheck";
                    object param = new 
                    {
                        @Reseller_ID = req.Reseller_ID,
                        @Product_Id = req.Product_Id,
                        @Plan_id = req.Plan_id,
                        @is_tms_flag = req.is_tms_flag
                    };
                    IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP");  
                if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new smeresellergetplaninfotestcheckOutput()
                        {
                            Bundle_Type = r.Bundle_Type,
                            Pc_price_discount = r.Pc_price_discount,
                            plan_name = r.plan_name,
                            PC_Product = r.PC_Product == null ? 0 : r.PC_Product,
                            PC_Price = r.PC_Price,
                            features = r.features,
                            National_min = r.National_min == null ? 0 : r.National_min,
                            Geo_Min = r.Geo_Min == null ? 0 : r.Geo_Min,
                            Request_ID = r.Request_ID == null ? 0 : r.Request_ID,
                            Unlimted_Intra_Call = r.Unlimted_Intra_Call == null ? 0 : r.Unlimted_Intra_Call,
                            is_free_trial = r.is_free_trial == null ? 0 : r.is_free_trial,
                            PC_most_popular = r.PC_most_popular == null ? 0 : r.PC_most_popular,
                            Audio_conf = r.Audio_conf == null ? 0 : r.Audio_conf,
                            Audio_conf_value = r.Audio_conf_value,
                            Video_conf = r.Video_conf == null ? 0 : r.Video_conf,
                            Video_conf_value = r.Video_conf_value,
                            Calling_UK_mins = r.Calling_UK_mins == null ? 0.0F : r.Calling_UK_mins,
                            created_date = r.created_date == null ? null : r.created_date,
                            created_by = r.created_by,
                            UserName = r.UserName,
                            status = r.status == null ? 0 : r.status,
                            freeph_non_geo_loc_min = r.freeph_non_geo_loc_min == null ? 0.0F : r.freeph_non_geo_loc_min,
                            Unlimited_inbound_conf = r.Unlimited_inbound_conf == null ? 0 : r.Unlimited_inbound_conf,
                            Unlimited_inbound_conf_value = r.Unlimited_inbound_conf_value == null ? 0.0F : r.Unlimited_inbound_conf_value,
                            reseller_discount_flag = r.reseller_discount_flag == null ? 0 : r.reseller_discount_flag,
                            is_reseller = r.is_reseller == null ? 0 : r.is_reseller,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        smeresellergetplaninfotestcheckOutput outputobj = new smeresellergetplaninfotestcheckOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                smeresellergetplaninfotestcheckOutput outputobj = new smeresellergetplaninfotestcheckOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }

    }
}