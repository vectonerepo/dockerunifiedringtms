﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UTMSDocker.Controllers;
using static UTMSDocker.Models.UtmsgetactiveplaninfoModel;

namespace UTMSDocker.DB
{
    public class UtmsgetactiveplaninfoDB
    {

        public static List<UtmsgetactiveplaninfoOutput> CallDB(IConfiguration _Config, ILogger _logger)
        {
            List<UtmsgetactiveplaninfoOutput> OutputList = new List<UtmsgetactiveplaninfoOutput>();


            var sp = "utms_get_active_plan_info";
            object param = new

            {

            };
            IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
            {
                OutputList.AddRange(result.Select(r => new UtmsgetactiveplaninfoOutput()
                {

                    Pln_Idx = r.Pln_Idx == null ? 0 : r.Pln_Idx,
                    Pln_Name = r.Pln_Name,
                    Code = r.errcode == null ? 0 : r.errcode,
                    Message = r.errmsg == null ? "Success" : r.errmsg,
                }));
            }
            else
            {
                UtmsgetactiveplaninfoOutput outputobj = new UtmsgetactiveplaninfoOutput();
                outputobj.Code = 1003;
                outputobj.Message = ErrorMessage.Errors[1003];
                OutputList.Add(outputobj);
            }
            _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(OutputList));
            return OutputList;

        }
    }
}