﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using UTMSDocker.Controllers;
using static UTMSDocker.Models.UtmsportalupdatebundlelistinfoModel;

namespace UTMSDocker.DB
{
    public class UtmsportalupdatebundlelistinfoDB
    {


        public static List<UtmsportalupdatebundlelistinfoOutput> CallDB(UtmsportalupdatebundlelistinfoInput req, IConfiguration _Config, ILogger _logger)
        {

            List<UtmsportalupdatebundlelistinfoOutput> OutputList = new List<UtmsportalupdatebundlelistinfoOutput>();
            _logger.LogInformation("Input : UtmsportalupdatebundlelistinfoController:" + JsonConvert.SerializeObject(req));
            try
            {

                var sp = "utms_portal_update_bundle_list_info";
                object param = new

                {
                    @bundle_name = req.bundle_name,
                    @price = req.price,
                    @national_min = req.national_min,
                    @international_min = req.international_min,
                    @bundleid = req.bundleid,
                    @bundle_status = req.bundle_status,
                    @bundle_type = req.bundle_type,
                    @reseller_flag = req.reseller_flag

                };
                IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
                {
                    _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                    OutputList.AddRange(result.Select(r => new UtmsportalupdatebundlelistinfoOutput()
                    {
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg
                    }));
                }
                else
                {
                    _logger.LogInformation("Output DB : " + "Empty result");
                    UtmsportalupdatebundlelistinfoOutput outputobj = new UtmsportalupdatebundlelistinfoOutput();
                    outputobj.Code = 1003;
                    outputobj.Message = ErrorMessage.Errors[1003];
                    OutputList.Add(outputobj);
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UtmsportalupdatebundlelistinfoOutput outputobj = new UtmsportalupdatebundlelistinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }

    }
}