﻿using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using static UTMSDocker.Models.UTMSupdateFreetrialsettingsinfoModel;
using Dapper;
using System.Data;
using UTMSDocker.Controllers;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UTMSupdateFreetrialsettingsinfoDB
    {
         

        public static List<UTMSupdateFreetrialsettingsinfoOutput> CallDB(UTMSupdateFreetrialsettingsinfoInput req,IConfiguration _Config,ILogger _logger)
        {
            List<UTMSupdateFreetrialsettingsinfoOutput> OutputList = new List<UTMSupdateFreetrialsettingsinfoOutput>();
            _logger.LogInformation("Input : UTMSupdateFreetrialsettingsinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
         
                    var sp = "UTMS_update_Free_trial_settings_info";
                    object param = new 
                    {
                        @Free_trial_id = req.Free_trial_id,
                        @Free_trial_day = req.Free_trial_day,
                        @Free_trial_status = req.Free_trial_status
                    };
                    IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP");  if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UTMSupdateFreetrialsettingsinfoOutput()
                        {
                            //Free_trial_status = r.Free_trial_status,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        UTMSupdateFreetrialsettingsinfoOutput outputobj = new UTMSupdateFreetrialsettingsinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UTMSupdateFreetrialsettingsinfoOutput outputobj = new UTMSupdateFreetrialsettingsinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }


    }
}