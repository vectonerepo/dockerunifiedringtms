﻿using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using static UTMSDocker.Models.UTMSValidateuserroleinfoModel;
using Dapper;
using System.Data;
using UTMSDocker.Controllers;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UTMSValidateuserroleinfoDB
    {
        public static List<UTMSValidateuserroleinfoOutput> CallDB(UTMSValidateuserroleinfoInput req,IConfiguration _Config,ILogger _logger)
        {
            List<UTMSValidateuserroleinfoOutput> OutputList = new List<UTMSValidateuserroleinfoOutput>();
            _logger.LogInformation("Input : UTMSValidateuserroleinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
               
                    var sp = "UTMS_Validate_user_role_info ";
                    object param = new 
                    {
                        @Userid = req.Userid
                    };
                    IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP");  
                    if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UTMSValidateuserroleinfoOutput()
                        {
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        UTMSValidateuserroleinfoOutput outputobj = new UTMSValidateuserroleinfoOutput();
                        outputobj.Code = 1003;
                        outputobj.Message = ErrorMessage.Errors[1003];
                        OutputList.Add(outputobj);
                    } 
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UTMSValidateuserroleinfoOutput outputobj = new UTMSValidateuserroleinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}