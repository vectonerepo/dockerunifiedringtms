﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using static UTMSDocker.Models.UtmsgettariffratedownloadModel;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UtmsgettariffratedownloadDB
    {
        public static List<UtmsgettariffratedownloadOutput> CallDB(UtmsgettariffratedownloadInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UtmsgettariffratedownloadOutput> OutputList = new List<UtmsgettariffratedownloadOutput>();
            _logger.LogInformation("Input : UtmsgettariffratedownloadController:" + JsonConvert.SerializeObject(req));
            try
            {
              
                var sp = "utms_get_tariff_rate_download";
                object param = new
                {
                    @trffclass = req.trffclass,
                    @accesscharge = req.accesscharge
                };
                IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
                {
                    _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                    OutputList.AddRange(result.Select(r => new UtmsgettariffratedownloadOutput()
                    {
                        destcode = r.destcode,
                        charge = r.charge == null ? 0 : r.charge,
                        timeperiod = r.timeperiod == null ? 0 : r.timeperiod,
                        cnxdelay = r.cnxdelay == null ? 0 : r.cnxdelay,
                        cnxprice = r.cnxprice == null ? 0.0D : r.cnxprice,
                        sampdelay = r.sampdelay == null ? 0 : r.sampdelay,
                        pricemin = r.pricemin == null ? 0.0D : r.pricemin,
                        premiumdest = r.premiumdest == null ? 0 : r.premiumdest,
                        freesec = r.freesec == null ? 0 : r.freesec,
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg
                    }));
                }
                else
                {
                    _logger.LogInformation("Output DB : " + "Empty result");
                    UtmsgettariffratedownloadOutput outputobj = new UtmsgettariffratedownloadOutput();
                    outputobj.Code = -1;
                    outputobj.Message = "No Rec found";
                    OutputList.Add(outputobj);
                }  
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UtmsgettariffratedownloadOutput outputobj = new UtmsgettariffratedownloadOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}