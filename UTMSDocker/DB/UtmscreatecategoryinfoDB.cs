﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using static UTMSDocker.Models.UtmscreatecategoryinfoModel;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
namespace UTMSDocker.DB
{
    public class UtmscreatecategoryinfoDB
    {


        public static List<UtmscreatecategoryinfoOutput> CallDB(UtmscreatecategoryinfoInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UtmscreatecategoryinfoOutput> OutputList = new List<UtmscreatecategoryinfoOutput>();
            _logger.LogInformation("Input : UtmscreatecategoryinfoController:" + JsonConvert.SerializeObject(req));
            try
            {

                var sp = "UTMS_create_Category_info";
                object param = new
                {

                    @Category_Name = req.Category_Name,
                    @process_type = req.process_type,
                    @Category_id = req.Category_id,
                    @status = req.status,
                    @category_position = req.category_position
                };
                IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
                {
                    _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                    OutputList.AddRange(result.Select(r => new UtmscreatecategoryinfoOutput()
                    {
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg
                    }));
                }
                else
                {
                    _logger.LogInformation("Output DB : " + "Empty result");
                    UtmscreatecategoryinfoOutput outputobj = new UtmscreatecategoryinfoOutput();
                    outputobj.Code = -1;
                    outputobj.Message = "No Rec found";
                    OutputList.Add(outputobj);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UtmscreatecategoryinfoOutput outputobj = new UtmscreatecategoryinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}