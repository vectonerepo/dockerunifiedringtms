﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using NLog.Fluent;
using UTMSDocker.Controllers;
using static UTMSDocker.Models.UTMSGetRoleInfoModel;

namespace UTMSDocker.DB
{
    public class UTMSGetRoleInfoDB
    {

        public static List<UTMSGetRoleInfoOutput> CallDB(UTMSGetRoleInfoInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UTMSGetRoleInfoOutput> OutputList = new List<UTMSGetRoleInfoOutput>();

            var sp = "UTMS_get_Role_Info";
            object param = new
            {
                @Pk_role_id = req.Pk_role_id
            };
            IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
            {
                OutputList.AddRange(result.Select(r => new UTMSGetRoleInfoOutput()
                {
                    Role_created_date = r.Role_created_date,
                    role_status = r.role_status == null ? 0 : r.role_status,
                    Pk_role_id = r.Pk_role_id,
                    Role_name = r.Role_name,
                    menu_id = r.menu_id,
                    Code = r.errcode == null ? 0 : r.errcode,
                    Message = r.errmsg == null ? "Success" : r.errmsg,
                }));
            }
            else
            {
                UTMSGetRoleInfoOutput outputobj = new UTMSGetRoleInfoOutput();
                outputobj.Code = 1003;
                outputobj.Message = ErrorMessage.Errors[1003];
                OutputList.Add(outputobj);
            }
            _logger.LogInformation("UTMSGetRoleInfo: " + JsonConvert.SerializeObject(OutputList));
            return OutputList;

        }
    }
}