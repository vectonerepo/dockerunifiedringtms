﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using static UTMSDocker.Models.UtmsuplaodesptariffratesModel;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UtmsuplaodesptariffratesDB
    { 
        public static List<UtmsuplaodesptariffratesOutput> CallDB(UtmsuplaodesptariffratesInput req,IConfiguration _Config,ILogger _logger)
        {
            List<UtmsuplaodesptariffratesOutput> OutputList = new List<UtmsuplaodesptariffratesOutput>();
            _logger.LogInformation("Input : UtmsuplaodesptariffratesController:" + JsonConvert.SerializeObject(req));
            try
            {
                
                  
                    var sp = "utms_uplaod_esp_tariff_rates";
                    object param = new
                    {
                        @login = req.login,
                        @tariffclass = req.tariffclass,
                        @access_type = req.access_type,
                        @effectivedate = req.effectivedate == null ? null : req.effectivedate,
                        @filename = req.filename,
                        @tempfilename = req.tempfilename,
                        @upload_type = req.upload_type
                    };
                           
                    IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); 
                if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UtmsuplaodesptariffratesOutput()
                        {
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        UtmsuplaodesptariffratesOutput outputobj = new UtmsuplaodesptariffratesOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                 
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UtmsuplaodesptariffratesOutput outputobj = new UtmsuplaodesptariffratesOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}