﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using static UTMSDocker.Models.UTMSgetcategoryfeatureinfoModel;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using UTMSDocker.Controllers;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UTMSgetcategoryfeatureinfoDB
    {

        public static List<UTMSgetcategoryfeatureinfoOutput> CallDB(UTMSgetcategoryfeatureinfoInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UTMSgetcategoryfeatureinfoOutput> OutputList = new List<UTMSgetcategoryfeatureinfoOutput>();

            var sp = "UTMS_get_category_feature_info";
            object param = new

            {
                @Category_id = req.Category_id
            };
            IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
            {
                OutputList.AddRange(result.Select(r => new UTMSgetcategoryfeatureinfoOutput()
                {
                    PF_Idx = r.PF_Idx == null ? 0 : r.PF_Idx,
                    PF_Name = r.PF_Name,
                    PF_Description = r.PF_Description,
                    code = r.code,
                    PF_category = r.PF_category,
                    last_upd_by = r.last_upd_by,
                    Category_id = r.Category_id == null ? 0 : r.Category_id,
                    PF_status = r.PF_status == null ? 0 : r.PF_status,
                    Code = r.errcode == null ? 0 : r.errcode,
                    Message = r.errmsg == null ? "Success" : r.errmsg,
                }));
            }
            else
            {
                UTMSgetcategoryfeatureinfoOutput outputobj = new UTMSgetcategoryfeatureinfoOutput();
                outputobj.Code = 1003;
                outputobj.Message = ErrorMessage.Errors[1003];
                OutputList.Add(outputobj);
            }
            _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(OutputList));
            return OutputList;

        }
    }
}