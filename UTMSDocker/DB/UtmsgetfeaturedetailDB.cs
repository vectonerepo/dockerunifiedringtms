﻿using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using static UTMSDocker.Models.UtmsgetfeaturedetailModel;
using Dapper;
using System.Data;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UtmsgetfeaturedetailDB
    {
         

        public static List<UtmsgetfeaturedetailOutput> CallDB(UtmsgetfeaturedetailInput req,IConfiguration _Config,ILogger _logger)
        {
            List<UtmsgetfeaturedetailOutput> OutputList = new List<UtmsgetfeaturedetailOutput>();
            _logger.LogInformation("Input : UtmsgetfeaturedetailController:" + JsonConvert.SerializeObject(req));
            try
            {
              
                    var sp = "UTMS_get_feature_detail";
                    object param = new 
                    {
                        @Feature_hdr = req.Feature_hdr,
                        @Feature_idx = req.Feature_idx
                    };
                    IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP");  if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UtmsgetfeaturedetailOutput()
                        {
                            PF_Idx = r.PF_Idx == null ? 0 : r.PF_Idx,
                            PF_Name = r.PF_Name,
                            PF_Description = r.PF_Description,
                            code = r.code,
                            PF_category = r.PF_category,
                            last_upd_by = r.last_upd_by,
                            Category_id = r.Category_id == null ? 0 : r.Category_id,
                            PF_status = r.PF_status == null ? 0: r.PF_status,
                            is_freetrial = r.is_freetrial == null ? 0 : r.is_freetrial,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg,
                            Feature_price = r.Feature_price == null ? 0.0D : r.Feature_price,
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        UtmsgetfeaturedetailOutput outputobj = new UtmsgetfeaturedetailOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    } 
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UtmsgetfeaturedetailOutput outputobj = new UtmsgetfeaturedetailOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}