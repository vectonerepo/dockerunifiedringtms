﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using static UTMSDocker.Models.UtmsuploadwebtariffratesInputModel;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UtmsuploadwebtariffratesDB
    {
         

        public static List<UtmsuploadwebtariffratesOutput> CallDB(UtmsuploadwebtariffratesInput req,IConfiguration _Config,ILogger _logger)
        {
            List<UtmsuploadwebtariffratesOutput> OutputList = new List<UtmsuploadwebtariffratesOutput>();
            _logger.LogInformation("Input : UtmsuploadwebtariffratesDB:" + JsonConvert.SerializeObject(req));
            try
            {
                
               // using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["UTMSUPLOADTARIFF"].ConnectionString))
                //{
                   // conn.Open();
                    var sp = "utms_upload_web_tariff_rates";
                    object param = new
                    {
                        @callingtype = req.callingtype,
                        @filename = req.filename,
                        @login_by = req.login_by
                    };
                           
                    IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSUPLOADTARIFF");  
                    if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UtmsuploadwebtariffratesOutput()
                        {                     
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        UtmsuploadwebtariffratesOutput outputobj = new UtmsuploadwebtariffratesOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UtmsuploadwebtariffratesOutput outputobj = new UtmsuploadwebtariffratesOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }

    }
}