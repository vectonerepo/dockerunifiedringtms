﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
//using UTMSDocker.Controllers;
//using static UTMSDocker.Controller.RequestTokenController;
using static UTMSDocker.Models.UtmsrolemenumappingModel;

namespace UTMSDocker.DB
{
    public class UtmsrolemenumappingDB
    {
        public static List<UtmsrolemenumappingOutput> CallDB(UtmsrolemenumappingInput req,IConfiguration _Config,ILogger _logger)
        {
            List<UtmsrolemenumappingOutput> OutputList = new List<UtmsrolemenumappingOutput>();
            _logger.LogInformation("Input : UtmsrolemenumappingController:" + JsonConvert.SerializeObject(req));
            try
            {
               
                    var sp = "UTMS_Role_Menu_Mapping";
                    object param = new
                    {
                        @Role_ID = req.Role_ID,
                        @menu_id = req.menu_id
                    };
                    IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP");  if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UtmsrolemenumappingOutput()
                        {
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        UtmsrolemenumappingOutput outputobj = new UtmsrolemenumappingOutput();
                        outputobj.Code = 1003;
                        outputobj.Message = UnifiedRingTMS.Controllers.ErrorMessage.Errors[1003];
                        OutputList.Add(outputobj);
                    }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UtmsrolemenumappingOutput outputobj = new UtmsrolemenumappingOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}