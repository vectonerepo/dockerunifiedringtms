﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using static UTMSDocker.Models.UTMSgetwebratedetailsModel;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UTMSgetwebratedetailsDB
    {
        public static List<UTMSgetwebratedetailsOutput> CallDB(UTMSgetwebratedetailsInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UTMSgetwebratedetailsOutput> OutputList = new List<UTMSgetwebratedetailsOutput>();
            _logger.LogInformation("Input : UTMSgetwebratedetailsDB :" + JsonConvert.SerializeObject(req));
            try
            {
                var sp = "UTMS_get_web_rate_details";
                object param = new
                {
                    @callingtype = req.callingtype
                };
                IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
                {
                    _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                    OutputList.AddRange(result.Select(r => new UTMSgetwebratedetailsOutput()
                    {
                        id = r.id == null ? 0 : r.id,
                        calling_type = r.calling_type,
                        country = r.country,
                        prefix = r.prefix,
                        fixed_rate = r.fixed_rate,
                        mobile_rate = r.mobile_rate,
                        Type = r.Type,
                        rate = r.rate,
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg,


                    }));
                }
                else
                {
                    _logger.LogInformation("Output DB : " + "Empty result");
                    UTMSgetwebratedetailsOutput outputobj = new UTMSgetwebratedetailsOutput();
                    outputobj.Code = -1;
                    outputobj.Message = "No Rec found";
                    OutputList.Add(outputobj);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UTMSgetwebratedetailsOutput outputobj = new UTMSgetwebratedetailsOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}