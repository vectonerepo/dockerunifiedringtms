﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using static UTMSDocker.Models.UtmsplanpricingcreatenotesinfoModel;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UtmsplanpricingcreatenotesinfoDB
    {
         
        public static List<UtmsplanpricingcreatenotesinfoOutput> CallDB(UtmsplanpricingcreatenotesinfoInput req,IConfiguration _Config,ILogger _logger)
        {
            List<UtmsplanpricingcreatenotesinfoOutput> OutputList = new List<UtmsplanpricingcreatenotesinfoOutput>();
            _logger.LogInformation("Input : UtmsplanpricingcreatenotesinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                
                    var sp = "utms_plan_pricing_create_notes_info";
                    object param = new
                    {
                        @plan_id = req.plan_id,
                        @processtype = req.processtype,
                        @plan_notes = req.plan_notes,
                        @plan_notes_status = req.plan_notes_status,
                        @plan_notes_id = req.plan_notes_id
                    };
                    IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP");  
                if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UtmsplanpricingcreatenotesinfoOutput()
                        {
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        UtmsplanpricingcreatenotesinfoOutput outputobj = new UtmsplanpricingcreatenotesinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                 
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UtmsplanpricingcreatenotesinfoOutput outputobj = new UtmsplanpricingcreatenotesinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}