﻿using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using System.Data;
using UTMSDocker.Controllers;
using static UTMSDocker.Models.UtmsrolemenumappinggetinfoModel;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UtmsrolemenumappinggetinfoDB
    {

        public static List<UtmsrolemenumappinggetinfoOutput> CallDB(UtmsrolemenumappinggetinfoInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UtmsrolemenumappinggetinfoOutput> OutputList = new List<UtmsrolemenumappinggetinfoOutput>();
            _logger.LogInformation("Input : UtmsrolemenumappinggetinfoController:" + JsonConvert.SerializeObject(req));
            try
            {

                var sp = "UTMS_Role_Menu_Mapping_get_info";
                object param = new
                {
                    @Role_ID = req.Role_ID,
                    @menu_id = req.menu_id
                };
                IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP");
                if (result != null && result.Count() > 0)
                {
                    _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                    OutputList.AddRange(result.Select(r => new UtmsrolemenumappinggetinfoOutput()
                    {
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg
                    }));
                }
                else
                {
                    _logger.LogInformation("Output DB : " + "Empty result");
                    UtmsrolemenumappinggetinfoOutput outputobj = new UtmsrolemenumappinggetinfoOutput();
                    outputobj.Code = -1;
                    outputobj.Message = "No Rec found";
                    OutputList.Add(outputobj);
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UtmsrolemenumappinggetinfoOutput outputobj = new UtmsrolemenumappinggetinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}