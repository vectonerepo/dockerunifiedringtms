﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using static UTMSDocker.Models.UTMScreateactivityloginfoModel;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UTMScreateactivityloginfoDB
    {


        public static List<UTMScreateactivityloginfoOutput> CallDB(UTMScreateactivityloginfoInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UTMScreateactivityloginfoOutput> OutputList = new List<UTMScreateactivityloginfoOutput>();
            _logger.LogInformation("Input : UTMSupdatepricesettingsinfoController:" + JsonConvert.SerializeObject(req));
            try
            {

                var sp = "UTMS_create_activity_log_info";
                object param = new
                {
                    @created_by = req.created_by,
                    @module_name = req.module_name,
                    @create_date = req.create_date,
                    @comments = req.comments
                };
                IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
                {
                    _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                    OutputList.AddRange(result.Select(r => new UTMScreateactivityloginfoOutput()
                    {
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg
                    }));
                }
                else
                {
                    _logger.LogInformation("Output DB : " + "Empty result");
                    UTMScreateactivityloginfoOutput outputobj = new UTMScreateactivityloginfoOutput();
                    outputobj.Code = -1;
                    outputobj.Message = "No Rec found";
                    OutputList.Add(outputobj);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UTMScreateactivityloginfoOutput outputobj = new UTMScreateactivityloginfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }  
    }
}