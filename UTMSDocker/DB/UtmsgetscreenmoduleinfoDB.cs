﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using static UTMSDocker.Models.UtmsgetscreenmoduleinfoModel;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UtmsgetscreenmoduleinfoDB
    {


        public static List<UtmsgetscreenmoduleinfoOutput> CallDB(UtmsgetscreenmoduleinfoInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UtmsgetscreenmoduleinfoOutput> OutputList = new List<UtmsgetscreenmoduleinfoOutput>();
            _logger.LogInformation("Input : UtmsgetscreenmoduleinfoController:" + JsonConvert.SerializeObject(req));
            try
            {

                var sp = "UTMS_get_screen_Module_info";
                object param = new
                {
                    @Module_id = req.Module_id
                };
                IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
                {
                    _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                    OutputList.AddRange(result.Select(r => new UtmsgetscreenmoduleinfoOutput()
                    {
                        Module_Id = r.Module_Id == null ? 0 : r.Module_Id,
                        Module_name = r.Module_name, //== null ? 0 : r.Module_name,
                        Status = r.Status == null ? 0 : r.Status,
                        Url = r.Url,
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg
                    }));
                }
                else
                {
                    _logger.LogInformation("Output DB : " + "Empty result");
                    UtmsgetscreenmoduleinfoOutput outputobj = new UtmsgetscreenmoduleinfoOutput();
                    outputobj.Code = -1;
                    outputobj.Message = "No Rec found";
                    OutputList.Add(outputobj);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UtmsgetscreenmoduleinfoOutput outputobj = new UtmsgetscreenmoduleinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}