﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using static UTMSDocker.Models.UTMScreateRoleMenuMappingModel;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UTMScreateRoleMenuMappingDB
    {


        public static List<UTMScreateRoleMenuMappingOutput> CallDB(UTMScreateRoleMenuMappingInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UTMScreateRoleMenuMappingOutput> OutputList = new List<UTMScreateRoleMenuMappingOutput>();
            _logger.LogInformation("Input : UTMScreateRoleMenuMappingController:" + JsonConvert.SerializeObject(req));
            try
            {
                var sp = "UTMS_create_Role_Menu_Mapping";
                object param = new
                {
                    @Role_Name = req.Role_Name,
                    @menu_id = req.menu_id,
                    @status = req.status,
                    @Role_ID = req.Role_ID,
                    @process_type = req.process_type
                };
                IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
                {
                    _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                    OutputList.AddRange(result.Select(r => new UTMScreateRoleMenuMappingOutput()
                    {
                        Role_created_date = r.Role_created_date,
                        role_status = r.role_status == null ? 0 : r.role_status,
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg
                    }));
                }
                else
                {
                    _logger.LogInformation("Output DB : " + "Empty result");
                    UTMScreateRoleMenuMappingOutput outputobj = new UTMScreateRoleMenuMappingOutput();
                    outputobj.Code = -1;
                    outputobj.Message = "No Rec found";
                    OutputList.Add(outputobj);
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UTMScreateRoleMenuMappingOutput outputobj = new UTMScreateRoleMenuMappingOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }

    }
}