﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using static UTMSDocker.Models.UtmscompanyupdatedeletecallingratedtlinfoModel;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UtmscompanyupdatedeletecallingratedtlinfoDB
    {



        public static List<UtmscompanyupdatedeletecallingratedtlinfoOutput> CallDB(UtmscompanyupdatedeletecallingratedtlinfoInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UtmscompanyupdatedeletecallingratedtlinfoOutput> OutputList = new List<UtmscompanyupdatedeletecallingratedtlinfoOutput>();
            _logger.LogInformation("Input : UtmscompanyupdatedeletecallingratedtlinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                // using (var conn = new SqlConnection(_Config["ConnectionStrings:UTMSAPP"]))
                //{
                // conn.Open();
                var sp = "utms_company_update_delete_calling_rate_dtl_info";
                object param = new

                {
                    @processtype = req.processtype,
                    @rate_id = req.rate_id,
                    @type = req.type,
                    @prefix = req.prefix,
                    @call_rate = req.call_rate,
                    @status = req.status,
                    @calling_id = req.calling_id,
                    @destination = req.destination
                };
                IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP");
                if (result != null && result.Count() > 0)
                {
                    _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                    OutputList.AddRange(result.Select(r => new UtmscompanyupdatedeletecallingratedtlinfoOutput()
                    {
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg
                    }));
                }
                else
                {
                    _logger.LogInformation("Output DB : " + "Empty result");
                    UtmscompanyupdatedeletecallingratedtlinfoOutput outputobj = new UtmscompanyupdatedeletecallingratedtlinfoOutput();
                    outputobj.Code = -1;
                    outputobj.Message = "No Rec found";
                    OutputList.Add(outputobj);
                }
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UtmscompanyupdatedeletecallingratedtlinfoOutput outputobj = new UtmscompanyupdatedeletecallingratedtlinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }

    }
}