﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using static UTMSDocker.Models.UtmsgetsmeplandetailsModel;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UtmsgetsmeplandetailsDB
    {
         
        public static List<UtmsgetsmeplandetailsOutput> CallDB(UtmsgetsmeplandetailsInput req,IConfiguration _Config,ILogger _logger)
        {
            List<UtmsgetsmeplandetailsOutput> OutputList = new List<UtmsgetsmeplandetailsOutput>();
            _logger.LogInformation("Input : UtmsgetsmeplandetailsDB:" + JsonConvert.SerializeObject(req));
            try
            {  
                    var sp = "utms_get_sme_plan_details";
                    object param = new
                    {
                        @Reseller_ID = req.Reseller_ID,
                        @Product_Id = req.Product_Id,
                        @Plan_id = req.Plan_id,
                        @is_tms_flag = req.is_tms_flag
                    };
                    IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP");  if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UtmsgetsmeplandetailsOutput()
                        {
                            Bundle_Type = r.Bundle_Type,
                            Pc_price_discount = r.Pc_price_discount,
                            plan_name = r.plan_name,
                            PC_Product = r.PC_Product == null ? 0 : r.PC_Product,
                            PC_Price = r.PC_Price,
                            features = r.features,
                            National_min = r.National_min == null ? 0 : r.National_min,
                            Geo_Min = r.Geo_Min == null ? 0 : r.Geo_Min,
                            Request_ID = r.Request_ID == null ? 0 : r.Request_ID,
                            Unlimted_Intra_Call = r.Unlimted_Intra_Call == null ? 0 : r.Unlimted_Intra_Call,
                            is_free_trial = r.is_free_trial == null ? 0 : r.is_free_trial,
                            PC_most_popular = r.PC_most_popular == null ? 0 : r.PC_most_popular,
                            Audio_conf = r.Audio_conf == null ? 0 : r.Audio_conf,
                            Audio_conf_value = r.Audio_conf_value,
                            Video_conf = r.Video_conf == null ? 0 : r.Video_conf,
                            Video_conf_value = r.Video_conf_value,
                            Calling_UK_mins = r.Calling_UK_mins == null ? 0.0F : r.Calling_UK_mins,
                            created_date = r.created_date == null ? null : r.created_date,
                            created_by = r.created_by,
                            UserName = r.UserName,
                            status = r.status == null ? 0 : r.status,
                            freeph_non_geo_loc_min = r.freeph_non_geo_loc_min == null ? 0.0F : r.freeph_non_geo_loc_min,
                            Unlimited_inbound_conf = r.Unlimited_inbound_conf == null ? 0 : r.Unlimited_inbound_conf,
                            Unlimited_inbound_conf_value = r.Unlimited_inbound_conf_value == null ? 0.0F : r.Unlimited_inbound_conf_value,
                            reseller_discount = r.reseller_discount == null ? 0.0F : r.reseller_discount,
                            is_reseller = r.is_reseller == null ? 0 : r.is_reseller,
                            free_trial_days = r.free_trial_days == null ? 0 : r.free_trial_days,
                            FT_audio_conf = r.FT_audio_conf == null ? 0 : r.FT_audio_conf,
                            FT_audio_conf_value = r.FT_audio_conf_value == null ? 0 : r.FT_audio_conf_value,
                            FT_video_conf = r.FT_video_conf == null ? 0 : r.FT_video_conf,
                            FT_video_conf_value = r.FT_video_conf_value == null ? 0 : r.FT_video_conf_value,
                            FT_Unlimted_Intra_Call = r.FT_Unlimted_Intra_Call == null ? 0 : r.FT_Unlimted_Intra_Call,
                            FT_Unlimted_Intra_Call_min = r.FT_Unlimted_Intra_Call_min == null ? 0 : r.FT_Unlimted_Intra_Call_min,
                            Free_trial_min = r.Free_trial_min == null ? 0 : r.Free_trial_min,
                            PC_From_Users = r.PC_From_Users == null ? 0 : r.PC_From_Users,
                            PC_To_Users = r.PC_To_Users == null ? 0 : r.PC_To_Users,
                            Video_conf_meetings = r.Video_conf_meetings,
                            Video_conf_participants = r.Video_conf_participants,
                            Audio_conf_participants = r.Audio_conf_participants,
                            FT_video_conf_meetings = r.FT_video_conf_meetings,
                            FT_video_conf_participants = r.FT_video_conf_participants,
                            FT_Audio_conf_participants = r.FT_Audio_conf_participants,
                            Video_conf_duration = r.Video_conf_duration == null ? 0 : r.Video_conf_duration,
                            MemberType_output = r.MemberType_output == null ? 0 : r.MemberType_output,
                            plan_show_output = r.plan_show_output == null ? 0 : r.plan_show_output,
                            addon_info = r.addon_info == null ? " " : r.addon_info,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg,
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        UtmsgetsmeplandetailsOutput outputobj = new UtmsgetsmeplandetailsOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    } 
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UtmsgetsmeplandetailsOutput outputobj = new UtmsgetsmeplandetailsOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}