﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using static UTMSDocker.Models.UtmsgetdownloadedfilenameModel;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UtmsgetdownloadedfilenameDB
    {


        public static List<UtmsgetdownloadedfilenameOutput> CallDB(UtmsgetdownloadedfilenameInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UtmsgetdownloadedfilenameOutput> OutputList = new List<UtmsgetdownloadedfilenameOutput>();
            _logger.LogInformation("Input : UtmsgetdownloadedfilenameController:" + JsonConvert.SerializeObject(req));
            try
            {

                var sp = "utms_get_downloaded_file_name";
                object param = new

                {
                    @callingtype = req.callingtype
                };
                IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
                {
                    _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                    OutputList.AddRange(result.Select(r => new UtmsgetdownloadedfilenameOutput()
                    {
                        filename = r.filename,
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg
                    }));
                }
                else
                {
                    _logger.LogInformation("Output DB : " + "Empty result");
                    UtmsgetdownloadedfilenameOutput outputobj = new UtmsgetdownloadedfilenameOutput();
                    outputobj.Code = -1;
                    outputobj.Message = "No Rec found";
                    OutputList.Add(outputobj);
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UtmsgetdownloadedfilenameOutput outputobj = new UtmsgetdownloadedfilenameOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}