﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using static UTMSDocker.Models.UTMSupdateDeleteWEBratedetailsModel;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UTMSupdateDeleteWEBratedetailsDB
    { 
        public static List<UTMSupdateDeleteWEBratedetailsOutput> CallDB(UTMSupdateDeleteWEBratedetailsInput req,IConfiguration _Config,ILogger _logger)
        {
            List<UTMSupdateDeleteWEBratedetailsOutput> OutputList = new List<UTMSupdateDeleteWEBratedetailsOutput>();
            _logger.LogInformation("Input : UTMSupdateDeleteWEBratedetails DB:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:UTMSAPP"]))
                //{
                   // conn.Open();
                    var sp = "UTMS_update_Delete_WEB_rate_details";
                    object param = new 
                    {
                        @id = req.id,
                        @fixed_Rate = req.fixed_Rate,
                        @mobile_rate = req.mobile_rate,
                        @procsstype = req.procsstype
                    };
                    IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP");  
                if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UTMSupdateDeleteWEBratedetailsOutput()
                        {
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        UTMSupdateDeleteWEBratedetailsOutput outputobj = new UTMSupdateDeleteWEBratedetailsOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    } 
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UTMSupdateDeleteWEBratedetailsOutput outputobj = new UTMSupdateDeleteWEBratedetailsOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }

    }
}