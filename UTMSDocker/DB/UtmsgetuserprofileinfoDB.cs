﻿using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using static UTMSDocker.Models.UtmsgetuserprofileinfoModelcs;
using Dapper;
using System.Data;
using UTMSDocker.Controllers;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UtmsgetuserprofileinfoDB
    {
        public static List<UtmsgetuserprofileinfoOutput> CallDB(UtmsgetuserprofileinfoInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UtmsgetuserprofileinfoOutput> OutputList = new List<UtmsgetuserprofileinfoOutput>();
            _logger.LogInformation("Input : UtmsgetuserprofileinfoController:" + JsonConvert.SerializeObject(req));
            try
            {

                var sp = "UTMS_get_user_profile_info";
                object param = new
                {
                    @User_ID = req.User_ID
                };
                IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
                {
                    _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                    OutputList.AddRange(result.Select(r => new UtmsgetuserprofileinfoOutput()
                    {
                        Userid = r.Userid == null ? 0 : r.Userid,
                        First_name = r.First_name,
                        Last_name = r.Last_name,
                        Username = r.Username,
                        Password = r.Password,
                        Fk_role_Id = r.Fk_role_Id == null ? 0 : r.Fk_role_Id,
                        last_login_ip = r.last_login_ip,
                        Role_name = r.Role_name,
                        User_status = r.User_status,
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg
                    }));
                }
                else
                {
                    _logger.LogInformation("Output DB : " + "Empty result");
                    UtmsgetuserprofileinfoOutput outputobj = new UtmsgetuserprofileinfoOutput();
                    outputobj.Code = 1003;
                    outputobj.Message = ErrorMessage.Errors[1003];
                    OutputList.Add(outputobj);
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UtmsgetuserprofileinfoOutput outputobj = new UtmsgetuserprofileinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}