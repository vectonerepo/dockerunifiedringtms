﻿using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using static UTMSDocker.Models.UTMSgetactivityloginfoModel;
using Dapper;
using System.Data;
using UTMSDocker.Controllers;
using Microsoft.Extensions.Configuration;

namespace UTMSDocker.DB
{
    public class UTMSgetactivityloginfoDB
    {



        public static List<UTMSgetactivityloginfoOutput> CallDB(UTMSgetactivityloginfoInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UTMSgetactivityloginfoOutput> OutputList = new List<UTMSgetactivityloginfoOutput>();
            _logger.LogInformation("Input : UTMSgetactivityloginfoController:" + JsonConvert.SerializeObject(req));
            try
            {

                var sp = "UTMS_get_activity_log_info";
                object param = new

                {
                    @From_date = req.From_date,
                    @to_date = req.to_date
                };
                IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
                {
                    _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                    OutputList.AddRange(result.Select(r => new UTMSgetactivityloginfoOutput()
                    {
                        created_by = r.created_by == null ? 0 : r.created_by,
                        Name = r.Name,
                        module_name = r.module_name,
                        create_date = r.create_date == null ? null : r.create_date,
                        comments = r.comments,
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg
                    }));
                }
                else
                {
                    _logger.LogInformation("Output DB : " + "Empty result");
                    UTMSgetactivityloginfoOutput outputobj = new UTMSgetactivityloginfoOutput();
                    outputobj.Code = -1;
                    outputobj.Message = "No Rec found";
                    OutputList.Add(outputobj);
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UTMSgetactivityloginfoOutput outputobj = new UTMSgetactivityloginfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }


    }
}