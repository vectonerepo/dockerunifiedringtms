﻿using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using System.Data;
//using UTMSDocker.Controllers;
using static UTMSDocker.Models.UTMScreatefeaturedetailModel;
using Microsoft.Extensions.Configuration;
//using static UTMSDocker.Controller.RequestTokenController;

namespace UTMSDocker.DB
{
    public class UTMScreatefeaturedetailDB
    {


        public static List<UTMScreatefeaturedetailOutput> CallDB(UTMScreatefeaturedetailInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UTMScreatefeaturedetailOutput> OutputList = new List<UTMScreatefeaturedetailOutput>();
            _logger.LogInformation("Input : UTMScreatefeaturedetailController:" + JsonConvert.SerializeObject(req));
            try
            {

                var sp = "UTMS_create_feature_detail";
                object param = new

                {
                    @feature_name = req.feature_name,
                    @feature_desc = req.feature_desc,
                    @code = req.code,
                    @Feature_hdr = req.Feature_hdr,
                    @process_type = req.process_type,
                    @last_upd_by = req.last_upd_by,
                    @Feature_idx = req.Feature_idx,
                    @Category_id = req.Category_id,
                    @status = req.status,
                    @is_freetrial = req.is_freetrial,
                    @Feature_price = req.Feature_price
                };
                IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:UTMSAPP"); if (result != null && result.Count() > 0)
                {
                    _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                    OutputList.AddRange(result.Select(r => new UTMScreatefeaturedetailOutput()
                    {

                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg
                    }));
                }
                else
                {
                    _logger.LogInformation("Output DB : " + "Empty result");
                    UTMScreatefeaturedetailOutput outputobj = new UTMScreatefeaturedetailOutput();
                    outputobj.Code = 1003;
                    outputobj.Message = UnifiedRingTMS.Controllers.ErrorMessage.Errors[1003];
                    OutputList.Add(outputobj);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UTMScreatefeaturedetailOutput outputobj = new UTMScreatefeaturedetailOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}